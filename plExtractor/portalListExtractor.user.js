// ==UserScript==
// @id             iitc-plugin-plExtractor
// @name           IITC plugin: Portal List Extractor
// @category       Controls
// @version        0.0.4.20161127.2217
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/j.bonnier/ingress_projects/raw/master/plExtractor/portalListExtractor.user.js
// @downloadURL    https://gitlab.com/j.bonnier/ingress_projects/raw/master/plExtractor/portalListExtractor.user.js
// @description    [iitc-2016-10-03-004740] Save your favorite Maps and Portals and move the intel map with a click. Works with sync.
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==

/*
 * Based on bookmarks-by-zaso.user.js
 * iitc-plugin-plExtractor@ZasoGD
 * IITC plugin: Bookmarks for maps and portals
 * 0.2.12.20161003.4740
 */

function wrapper(plugin_info) {
// ensure plugin framework is there, even if iitc is not yet loaded
if(typeof window.plugin !== 'function') window.plugin = function() {};

//PLUGIN AUTHORS: writing a plugin outside of the IITC build environment? if so, delete these lines!!
//(leaving them in place might break the 'About IITC' page or break update checks)
plugin_info.buildName = 'iitc';
plugin_info.dateTimeVersion = '20161127.2217';
plugin_info.pluginId = 'plExtractor';
//END PLUGIN AUTHORS NOTE



// PLUGIN START ////////////////////////////////////////////////////////
/***********************************************************************

  HOOKS:
  - pluginPlExtractorEdit: fired when a portal/folder is removed, added or sorted, also when a folder is opened/closed;
  - pluginPlExtractorOpenOpt: fired when the "Portal List Extractor Options" panel is opened (you can add new options);
  - pluginPlExtractorSyncEnd: fired when the sync is finished;

***********************************************************************/
////////////////////////////////////////////////////////////////////////

  // use own namespace for plugin
  window.plugin.plExtractor = function() {};

  window.plugin.plExtractor.SYNC_DELAY = 5000;

  window.plugin.plExtractor.KEY_OTHER_PRTL = 'idOthers';
  window.plugin.plExtractor.KEY_STORAGE = 'plugin-plExtractor';
  window.plugin.plExtractor.KEY_STATUS_BOX = 'plugin-plExtractor-box';

  window.plugin.plExtractor.KEY = {key: window.plugin.plExtractor.KEY_STORAGE, field: 'prtlsObj'};
  window.plugin.plExtractor.UPDATE_QUEUE = {key: 'plugin-plExtractor-queue', field: 'updateQueue'};
  window.plugin.plExtractor.UPDATING_QUEUE = {key: 'plugin-plExtractor-updating-queue', field: 'updatingQueue'};

  window.plugin.plExtractor.prtlsObj = {};
  window.plugin.plExtractor.statusBox = {};
  window.plugin.plExtractor.updateQueue = {};
  window.plugin.plExtractor.updatingQueue = {};

  window.plugin.plExtractor.enableSync = false;

  window.plugin.plExtractor.starLayers = {};
  window.plugin.plExtractor.starLayerGroup = null;

  window.plugin.plExtractor.isSmart = undefined;
  window.plugin.plExtractor.isAndroid = function() {
    if(typeof android !== 'undefined' && android) {
      return true;
    }
    return false;
  };

/* DEBUG **********************************/
  window.plugin.plExtractor.DEBUG = false;
  window.plugin.plExtractor.LINE_ADD = 27;
  window.plugin.plExtractor.getCurrentLine = function() {
    try {
        throw Error('');
    } catch(err) {
        return parseInt(err.stack.split('\n')[2].split(':')[1]) + window.plugin.plExtractor.LINE_ADD;
    }
  };


/*********************************************************************************************************************/

  // Generate an ID for the portal (date time + random number)
  window.plugin.plExtractor.generateID = function() {
    var d = new Date();
    var ID = d.getTime()+(Math.floor(Math.random()*99)+1);
    var ID = 'id'+ID.toString();
    return ID;
  };

  // Format the string
  window.plugin.plExtractor.escapeHtml = function(text) {
    return text
        .replace(/&/g, "&amp;")
        .replace(/</g, "&lt;")
        .replace(/>/g, "&gt;")
        .replace(/"/g, "&quot;")
        .replace(/'/g, "&#039;")
        .replace(/\//g, '&#47;')
        .replace(/\\/g, '&#92;');
  };

  // Update the localStorage
  window.plugin.plExtractor.saveStorage = function() {
    localStorage[plugin.plExtractor.KEY_STORAGE] = JSON.stringify(window.plugin.plExtractor.prtlsObj);
  };
  // Load the localStorage
  window.plugin.plExtractor.loadStorage = function() {
    window.plugin.plExtractor.prtlsObj = JSON.parse(localStorage[plugin.plExtractor.KEY_STORAGE]);
  };

  window.plugin.plExtractor.saveStorageBox = function() {
    localStorage[plugin.plExtractor.KEY_STATUS_BOX] = JSON.stringify(window.plugin.plExtractor.statusBox);
  };
  window.plugin.plExtractor.loadStorageBox = function() {
    window.plugin.plExtractor.statusBox = JSON.parse(localStorage[plugin.plExtractor.KEY_STATUS_BOX]);
  };

  window.plugin.plExtractor.upgradeToNewStorage = function() {
    if(localStorage['plugin-plExtractor-portals-data'] && localStorage['plugin-plExtractor-maps-data']) {
      // DELETE
      //var oldStor_1 = JSON.parse(localStorage['plugin-plExtractor-maps-data']);
      var oldStor_2 = JSON.parse(localStorage['plugin-plExtractor-portals-data']);

      window.plugin.plExtractor.prtlsObj.maps = oldStor_1.prtl_maps;
      window.plugin.plExtractor.prtlsObj.portals = oldStor_2.prtl_portals;
      window.plugin.plExtractor.saveStorage();

      // DELETE
      //localStorage.removeItem('plugin-plExtractor-maps-data');
      localStorage.removeItem('plugin-plExtractor-portals-data');
      localStorage.removeItem('plugin-plExtractor-status-box');
    }
  };

  window.plugin.plExtractor.createStorage = function() {
    if(!localStorage[window.plugin.plExtractor.KEY_STORAGE]) {
      // DELETE
      //window.plugin.plExtractor.prtlsObj.maps = {idOthers:{label:"Others",state:1,prtl:{}}};
      window.plugin.plExtractor.prtlsObj.portals = {idOthers:{label:"Others",state:1,prtl:{}}};
      window.plugin.plExtractor.saveStorage();
    }
    if(!localStorage[window.plugin.plExtractor.KEY_STATUS_BOX]) {
      window.plugin.plExtractor.statusBox.show = 1;
      window.plugin.plExtractor.statusBox.page = 0;
      window.plugin.plExtractor.statusBox.pos = {x:100,y:100};
      window.plugin.plExtractor.saveStorageBox();
    }
  };

  window.plugin.plExtractor.refreshPrtls = function() {
    $('#prtl_maps > ul, #prtl_portals > ul').remove();

    window.plugin.plExtractor.loadStorage();
    window.plugin.plExtractor.loadList('maps');
    window.plugin.plExtractor.loadList('portals');

    window.plugin.plExtractor.updateStarPortal();
    window.plugin.plExtractor.jquerySortableScript();
  };

/***************************************************************************************************************************************************************/

  // Show/hide the portal list box
  window.plugin.plExtractor.switchStatusPrtlsBox = function(status) {
    var newStatus = status;

    if(newStatus === 'switch') {
      if(window.plugin.plExtractor.statusBox.show === 1) {
        newStatus = 0;
      } else {
        newStatus = 1;
      }
    }

    if(newStatus === 1) {
      $('#plExtractorBox').css('height', 'auto');
      $('#prtlsTrigger').css('height', '0');
    } else {
      $('#prtlsTrigger').css('height', '64px');
      $('#plExtractorBox').css('height', '0');
    }

    window.plugin.plExtractor.statusBox['show'] = newStatus;
    window.plugin.plExtractor.saveStorageBox();
  };

  window.plugin.plExtractor.onPaneChanged = function(pane) {
    if(pane === "plugin-plExtractor")
      $('#plExtractorBox').css("display", "");
    else
      $('#plExtractorBox').css("display", "none");
  };

  // Switch list (maps/portals)
  window.plugin.plExtractor.switchPagePrtlsBox = function(elem, page) {
    window.plugin.plExtractor.statusBox.page = page;
    window.plugin.plExtractor.saveStorageBox();

    $('h5').removeClass('current');
    $(elem).addClass('current');

    var sectList = '#'+$(elem).attr('class').replace(' current', '');
    $('#plExtractorBox .portalList').removeClass('current');
    $(sectList).addClass('current');
  };

  // Switch the status folder to open/close (in the localStorage)
  window.plugin.plExtractor.openFolder = function(elem) {
    $(elem).parent().parent('li').toggleClass('open');

    var typeList = $(elem).parent().parent().parent().parent('div').attr('id').replace('prtl_', '');
    var ID = $(elem).parent().parent('li').attr('id');

    var newFlag;
    var flag = window.plugin.plExtractor.prtlsObj[typeList][ID]['state'];
    if(flag) { newFlag = 0; }
    else if(!flag) { newFlag = 1; }

    window.plugin.plExtractor.prtlsObj[typeList][ID]['state'] = newFlag;
    window.plugin.plExtractor.saveStorage();
    window.runHooks('pluginPlExtractorEdit', {"target": "folder", "action": newFlag?"open":"close", "id": ID});
  };

  // Load the HTML portals
  window.plugin.plExtractor.loadList = function(typeList) {
    var element = '';
    var elementTemp = '';
    var elementExc = '';
    var returnToMap = '';

    if(window.plugin.plExtractor.isSmart) {
      returnToMap = 'window.show(\'map\');';
    }

    // For each folder
    var list = window.plugin.plExtractor.prtlsObj[typeList];

    for(var idFolders in list) {
      var folders = list[idFolders];
      var active = '';

      // Create a label and a anchor for the sortable
      var folderDelete = '<span class="folderLabel"><a class="plExtractorRemoveFrom" onclick="window.plugin.plExtractor.removeElement(this, \'folder\');return false;" title="Remove this folder">X</a>';
      var folderName = '<a class="plExtractorAnchor" onclick="window.plugin.plExtractor.openFolder(this);return false"><span></span>'+folders['label']+'</a></span>';//<span><span></span></span>';
      var folderLabel = folderDelete+folderName;

      if(folders['state']) { active = ' open'; }
      if(idFolders === window.plugin.plExtractor.KEY_OTHER_PRTL) {
        folderLabel = '';
        active= ' othersPlExtractor open';
      }
      // Create a folder
      elementTemp = '<li class="portalFolder'+active+'" id="'+idFolders+'">'+folderLabel+'<ul>';

      // For each portal
      var fold = folders['prtl'];
      for(var idPrtl in fold) {
        var btn_link;
        var btn_remove = '<a class="plExtractorRemoveFrom" onclick="window.plugin.plExtractor.removeElement(this, \''+typeList+'\');return false;" title="Remove from prtal list">X</a>';

        var btn_move = '';
        if(window.plugin.plExtractor.isSmart) {
          btn_move = '<a class="plExtractorMoveIn" onclick="window.plugin.plExtractor.dialogMobileSort(\''+typeList+'\', this);return false;">=</a>';
        }

        var prtl = fold[idPrtl];
        var label = prtl['label'];
        var latlng = prtl['latlng'];


        // DELETE
        /*//  If it's a map
        if(typeList === 'maps') {
          if(prtl['label']=='') { label = prtl['latlng']+' ['+prtl['z']+']'; }
          btn_link = '<a class="plExtractorLink" onclick="'+returnToMap+'window.map.setView(['+latlng+'], '+prtl['z']+');return false;">'+label+'</a>';
        }
        // If it's a portal
        else if(typeList === 'portals') {*/
        var guid = prtl['guid'];
        var btn_link = '<a class="plExtractorLink" onclick="$(\'a.plExtractorLink.selected\').removeClass(\'selected\');'+returnToMap+'window.zoomToAndShowPortal(\''+guid+'\', ['+latlng+']);return false;">'+label+'</a>';
        //}
        // Create the bookmark
        elementTemp += '<li class="prtl" id="'+idPrtl+'">'+btn_remove+btn_move+btn_link+'</li>';
      }
      elementTemp += '</li></ul>';

      // Add folder 'Others' in last position
      if(idFolders !== window.plugin.plExtractor.KEY_OTHER_PRTL) { element += elementTemp; }
      else{ elementExc = elementTemp; }
    }
    element += elementExc;
    element = '<ul>'+element+'</ul>';

    // Append all folders and portals
    $('#prtl_'+typeList).append(element);
  };

/***************************************************************************************************************************************************************/

  window.plugin.plExtractor.findByGuid = function(guid) {
    var list = window.plugin.plExtractor.prtlsObj['portals'];

    for(var idFolders in list) {
      for(var idPrtl in list[idFolders]['prtl']) {
        var portalGuid = list[idFolders]['prtl'][idPrtl]['guid'];
        if(guid === portalGuid) {
          return {"id_folder":idFolders,"id_portal":idPrtl};
        }
       }
    }
    return;
  };

  // Append a 'star' flag in sidebar.
  window.plugin.plExtractor.onPortalSelectedPending = false;
  window.plugin.plExtractor.onPortalSelected = function() {
    $('.prtlsStar').remove();

    if(window.selectedPortal === null) return;

    if (!window.plugin.plExtractor.onPortalSelectedPending) {
      window.plugin.plExtractor.onPortalSelectedPending = true;

      setTimeout(function() { // the sidebar is constructed after firing the hook
        window.plugin.plExtractor.onPortalSelectedPending = false;

        $('.prtlsStar').remove();

        if(typeof(Storage) === "undefined") {
          $('#portaldetails > .imgpreview').after(plugin.plExtractor.htmlDisabledMessage);
          return;
        }

        // Prepend a star to mobile status-bar
        if(window.plugin.plExtractor.isSmart) {
          $('#updatestatus').prepend(plugin.plExtractor.htmlStar);
          $('#updatestatus .prtlsStar').attr('title', '');
        }

        $('#portaldetails > h3.title').before(plugin.plExtractor.htmlStar);
        window.plugin.plExtractor.updateStarPortal();
      }, 0);
    }

  };

  // Update the status of the star (when a portal is selected from the portal-list)
  window.plugin.plExtractor.updateStarPortal = function() {
    var guid = window.selectedPortal;
    $('.prtlsStar').removeClass('favorite');
    $('.prtl a.plExtractorLink.selected').removeClass('selected');

    // If current portal is into portals: select portal from portals list and select the star
    if(localStorage[window.plugin.plExtractor.KEY_STORAGE].search(guid) !== -1) {
      var prtlData = window.plugin.plExtractor.findByGuid(guid);
      if(prtlData) {
        $('.prtl#'+prtlData['id_portal']+' a.plExtractorLink').addClass('selected');
        $('.prtlsStar').addClass('favorite');
      }
    }
  };

  // Switch the status of the star
  window.plugin.plExtractor.switchStarPortal = function(guid) {
    if(guid === undefined) guid = window.selectedPortal;

    // If portal is saved in bookmarks: Remove this bookmark
    var prtlData = window.plugin.plExtractor.findByGuid(guid);
    if(prtlData) {
      var list = window.plugin.plExtractor.prtlsObj['portals'];
      delete list[prtlData['id_folder']]['prtl'][prtlData['id_portal']];
      $('.prtl#'+prtlData['id_portal']+'').remove();

      window.plugin.plExtractor.saveStorage();
      window.plugin.plExtractor.updateStarPortal();

      window.runHooks('pluginPlExtractorEdit', {"target": "portal", "action": "remove", "folder": prtlData['id_folder'], "id": prtlData['id_portal'], "guid":guid});
      console.log('PL EXTRACTOR: removed portal ('+prtlData['id_portal']+' situated in '+prtlData['id_folder']+' folder)');
    }
    // If portal isn't saved in portal list: Add this portal
    else{
      // Get portal name and coordinates
      var p = window.portals[guid];
      var ll = p.getLatLng();
      var currentdate = new Date();
      var currenttime = currentdate.getFullYear() + ""
                        + (currentdate.getMonth()+1) + ""
                        + currentdate.getDate() + "."  
                        + currentdate.getHours() + "."  
                        + currentdate.getMinutes() + "." 
                        + currentdate.getSeconds();
      // TODO ajouter d'autres champs
      plugin.plExtractor.addPortalExtract(
                guid,
                ll.lat+','+ll.lng,
                p.options.data.title,
                p.options.data['image'],
                p.options.data['team'],
                p.options.data['resCount'],
                p.options.data['level'],
                p.options.data['mission'],
                p.options.data['health'],
                p.options.data['artifactBrief'],
                p.options.data['ornaments'],
                p.options.data['timestamp'],
                currenttime);
                
    }
  };
      
  // TODO ajouter d'autres champs
  plugin.plExtractor.addPortalExtract = function(guid, latlng, label, image, team, resCount, level, mission, health, artifactBrief, ornaments, timestamp, currenttime) {
    var ID = window.plugin.plExtractor.generateID();
    // Add portal in the localStorage
    window.plugin.plExtractor.prtlsObj['portals'][window.plugin.plExtractor.KEY_OTHER_PRTL]['prtl'][ID] = {
        "guid":guid,
        "latlng":latlng,
        "label":label,
        "image":image,
        "team":team,
        "resCount":resCount,
        "level":level,
        "mission":mission,
        "health":health,
        "artifactBrief":artifactBrief,
        "ornaments":ornaments,
        "timestamp":timestamp,
        "currenttime":currenttime
    };

    window.plugin.plExtractor.saveStorage();
    window.plugin.plExtractor.refreshPrtls();
    window.runHooks('pluginPlExtractorEdit', {"target": "portal", "action": "add", "id": ID, "guid": guid});
    console.log('PL EXTRACTOR: added portal '+ID);
  };

  // Add PORTAL/FOLDER
  window.plugin.plExtractor.addElement = function(elem, type) {
    var ID = window.plugin.plExtractor.generateID();
    var typeList = $(elem).parent().parent('div').attr('id');

    // Get the label | Convert some characters | Set the input (empty)
    var input = '#'+typeList+' .addForm input';
    var label = $(input).val();
    label = window.plugin.plExtractor.escapeHtml(label);
    $(input).val('');

    if(label === '') { label = 'Folder'; }
    var short_type = typeList.replace('prtl_', '');
    // Add new folder in the localStorage
    window.plugin.plExtractor.prtlsObj[short_type][ID] = {"label":label,"state":1,"prtl":{}};
    window.plugin.plExtractor.saveStorage();
    window.plugin.plExtractor.refreshPrtls();
    window.runHooks('pluginPlExtractorEdit', {"target": type, "action": "add", "id": ID});
    console.log('PL EXTRACTOR: added '+type+' '+ID);
  };

  // Remove PORTAL/FOLDER
  window.plugin.plExtractor.removeElement = function(elem, type) {
    // DELETE maps
    if(/*type === 'maps' ||*/ type === 'portals') {
      var typeList = $(elem).parent().parent().parent().parent().parent('div').attr('id');
      var ID = $(elem).parent('li').attr('id');
      var IDfold = $(elem).parent().parent().parent('li').attr('id');
      var guid = window.plugin.plExtractor.prtlsObj[typeList.replace('prtl_', '')][IDfold]['prtl'][ID].guid;

      delete window.plugin.plExtractor.prtlsObj[typeList.replace('prtl_', '')][IDfold]['prtl'][ID];
      $(elem).parent('li').remove();

      // DELETE
      //if(type === 'portals') {
      var list = window.plugin.plExtractor.prtlsObj['portals'];

      window.plugin.plExtractor.updateStarPortal();
      window.plugin.plExtractor.saveStorage();

      window.runHooks('pluginPlExtractorEdit', {"target": "portal", "action": "remove", "folder": IDfold, "id": ID, "guid": guid});
      console.log('PL EXTRACTOR: removed portal ('+ID+' situated in '+IDfold+' folder)');
      // DELETE
      /*} else {
        window.plugin.plExtractor.saveStorage();
        window.runHooks('pluginPlExtractorEdit', {"target": "map", "action": "remove", "id": ID});
        console.log('PL EXTRACTOR: removed map '+ID);
      }*/
    }
    else if(type === 'folder') {
      var typeList = $(elem).parent().parent().parent().parent('div').attr('id');
      var ID = $(elem).parent().parent('li').attr('id');

      delete plugin.plExtractor.prtlsObj[typeList.replace('prtl_', '')][ID];
      $(elem).parent().parent('li').remove();
      window.plugin.plExtractor.saveStorage();
      window.plugin.plExtractor.updateStarPortal();
      window.runHooks('pluginPlExtractorEdit', {"target": "folder", "action": "remove", "id": ID});
      console.log('PL EXTRACTOR: removed folder '+ID);
    }
  };

  window.plugin.plExtractor.deleteMode = function() {
    $('#plExtractorBox').removeClass('moveMode').toggleClass('deleteMode');
  };

  window.plugin.plExtractor.moveMode = function() {
    $('#plExtractorBox').removeClass('deleteMode').toggleClass('moveMode');
  };

  window.plugin.plExtractor.mobileSortIDb = '';
  window.plugin.plExtractor.mobileSortIDf = '';
  window.plugin.plExtractor.dialogMobileSort = function(type, elem){
    window.plugin.plExtractor.mobileSortIDb = $(elem).parent('li.prtl').attr('id');
    window.plugin.plExtractor.mobileSortIDf = $(elem).parent('li.prtl').parent('ul').parent('li.portalFolder').attr('id');

    if(type === 'maps'){ type = 1; }
    else if(type === 'portals'){ type = 2; }

    dialog({
      html: window.plugin.plExtractor.dialogLoadListFolders('plExtractorDialogMobileSort', 'window.plugin.plExtractor.mobileSort', true, type),
      dialogClass: 'ui-dialog-prtlsSet-copy',
      title: 'Portl List Extractor - Move Portal'
    });
  };

  window.plugin.plExtractor.mobileSort = function(elem){
    var type = $(elem).data('type');
    var idPrtl = window.plugin.plExtractor.mobileSortIDb;
    var newFold = $(elem).data('id');
    var oldFold = window.plugin.plExtractor.mobileSortIDf;

    var Prtl = window.plugin.plExtractor.prtlsObj[type][oldFold].prtl[idPrtl];

    delete window.plugin.plExtractor.prtlsObj[type][oldFold].prtl[idPrtl];

    window.plugin.plExtractor.prtlsObj[type][newFold].prtl[idPrtl] = Prtl;

    window.plugin.plExtractor.saveStorage();
    window.plugin.plExtractor.refreshPrtls();
    window.runHooks('pluginPlExtractorEdit', {"target": "plExtractor", "action": "sort"});
    window.plugin.plExtractor.mobileSortIDf = newFold;
    console.log('Move Portal List '+type+' ID:'+idPrtl+' from folder ID:'+oldFold+' to folder ID:'+newFold);
  };

  window.plugin.plExtractor.onSearch = function(query) {
    var term = query.term.toLowerCase();
    // DELETE
    /*
    $.each(plugin.plExtractor.prtlsObj.maps, function(id, folder) {
      $.each(folder.prtl, function(id, portal) {
        if(portal.label.toLowerCase().indexOf(term) === -1) return;
        
        // TODO ajouter des champs ?
        query.addResult({
          title: escapeHtmlSpecialChars(portal.label),
          description: 'Map in folder "' + escapeHtmlSpecialChars(folder.label) + '"',
          // TODO changer l'icone.
          icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAADYSURBVCiRldExLoRxEAXw33xZ4QIKB9BQWPl0yFKJA4hCo1CqXMABVA6gcgkFnc7/s7VQaByAgoYdhU3sZr9NmGSaee/NvJeJUkr6R3WgrusYm/ajJ7zr5t3ouGmarFrXpFPpuA2aFDSxIWxjXz/mWy25jx3hEAsqS0NsFi/68YxHlXPK8MKbGwR6GN06g0XhwYrrX0tb+enJAS5b8pzp5gk5GM+wl1/C1YQgfEwPPbA+JN3iAgMsTxeEOWlXNzet5pHKGl7HOKWUzEx/6VJKdvj54IT3KfUNvrNZ/jYm+uoAAAAASUVORK5CYII=',
          position: L.latLng(portal.latlng.split(",")),
          zoom: portal.z,
          onSelected: window.plugin.plExtractor.onSearchResultSelected
        });
      });
    });*/

    $.each(plugin.plExtractor.prtlsObj.portals, function(id, folder) {
      $.each(folder.prtl, function(id, portal) {
        if(portal.label.toLowerCase().indexOf(term) === -1) return;
        
        // TODO ajouter des champs?
        query.addResult({
          title: escapeHtmlSpecialChars(portal.label),
          description: 'Portal in folder "' + escapeHtmlSpecialChars(folder.label) + '"',
          icon: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAoCAYAAACfKfiZAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAB79JREFUeNq8l2tsHNUVx3/33nnv+rGO105im7wDzftRkoaXgqp+aMRDrfpAQiofikRQaRIapRRaUKmKS9SHCuqXopZUCpWST5US1A9Vm9YIlYRIEKCNQgpUQKAktrMbr3e9szNzbz/cdew8sJ1Au9JoRrMz5/zO/5x7zh1hjOFKfv23iVuAu4BbgCWABzSAfwEvAPseed68MFN7YqYA/beJDQjxNLARBACRBx2RQEkYjQ3DVQADcARjtj3yvHn5UwHov109BDyBECrnCTbMl/QVJIVIEDgCg6GeQKmm+c85w3slzfCozgKH79/zXLb7EwH03+78EiG2g+DmxYoVPQ6tgURJAUJMPGgMYEi1YaSW8WE54/SIRgnz1Oankx1XBdB/h/sQiCcRkjvXuCzucvEdCUICopkJYWU3TfmNxhhNnGjOVjNGaimeNN9b+MP67isC6L/D24SQLyKEvHN1wNLZHu64cyGb0U9SAGNVMNpCaE0jTak3MoTJdOBkN3nbai/NCKD/Tl+COIaQK2+9LmDdfB/fdUAqEGqSApemwAJkoDOMzsiyDEdkIOQbGL2G+4b0ZF/O5RMj70bIlb0Fl2W9Ib7vgXSaztUkFRQoD4QDJoG0DjoDk4LMEDpFiRTQoLyVCHk3sHcGAGoHQnL9ooiWXAjKaQJcpIIKIOoCv2BIq4LaGWiMQBpbIKEQQoFOQHrghDsuBpCXyP+l/DKEWtfT4dEzK0I6vnWkAnACcMKJs5uHqBu6N/yd3lufJzcHwlngt4ITNd/zMdK3QbjROvZ/btnUCgi5BSG5dm5EPhdYiZUL0rVRKA/cnHXutUG+B/zWlCVffQ6/7U3ePrAVFeRoVCAZhTRGCDEpXWoLcHwKAHUDQtHVHtjoZRNABeC1WslzcyEogJNrKpKz7y7+ylHaFr3LP559EDm4wKZrFFJhCxMJTnTDdAqsyIcurblgImInhKADWhdC6zwIOkeIOv9C2H2MqOtD/LbR8+8X157hpp88xss//gZDr38BIa16mbF140TLpwO4ZnbBJxf5E5H7bdC2CNqXQEvfAN0bfoebG/vYDubmE2588rcv7Xv8RPbOH7+1slfJNr9ZyE4wbzoAvz3n4fv+eOVayVv6IJrzV3o3PwNi2gEyWh7y3nj9lTWlD87J+cUibaEcV9OfBkDhew5yvPD8Vgi7wIlO03PjsyBMafCD8NUXDi4tDZ6aXa9WWjZev+7dxbfcc3TcxFuvvVg8+JvHdo5Vhub3zsoT+B4oAyq0ik4NIEpCOQWDQijXVrt0wWs5kGr0vp898LVTbx/foo0JEIJrr+mk2x0aAI4C/O0Pv1515E+/35Y1xvJzO/NsXlmgvUWASMENQXmlaRqRPJ1kFFIkrnRBuNA4l6Wd1x9+5vF7vlMeOrMeoVjQ08mqpXNZvmgOdRFgtBb7f/XwHe8cP3IXOhNrFnezcWkrhVAjdd26ciJQ/unpFHitGpvr4lTgSgeyGNLa23/e9/MvlgdPr/d9n9tu/gzzeooEYQhC8s6pUjhw4Js7Smfe3wiCz69fwIp5eSKVIrI6GGVVdPMgvdema8VHRsb012uxJm80pDXeO5tGox999OVFPQXWLetjQV8Rxw1AOFTrKUltZEObFzPrmk5WLexgQVeELxNINWgxvv5tv5DOkSkBWgJ1sJ7oX4w1MjCCRqoJZKN37ZIiUS5HR6EdRyk7CYXEDwIW9hbpKebwpCZyNQ4JpGlzXAtb/V6b7aAmOzjlLPj2nn+/pSSHK2Mp2mS4SlDIe/QVc3S3R/iuvGD2O0LTErl0tkW05jwcJSZGM83V6oR2RjjBYT770FtTAgC0R85T56oJtThFAK4CzxFIYUBrO3J1aqecbjTPafO+Pr8pwWjbfIIO8NtBqKcuKfnLAcxuV/vjJD1WrtTtbDd6Ys6b9ELnWeNCiPPPZFYBNwdREZzwGLqxf0YAmx45ZkKHHeVKTD2Om4YT62TcYdawK+T8Mem+TiyEcKz0XgEwO1iz3cwIAGDTw0cH6nFjz1C5hskakKWTnDSdpheds3gCwmjwWiAsgnT3sPzegcv5kVP1c0/pXWfP1YYq1dqFUacxZHV7pPVJ1+MQia38sAhONIROdn2cjykBVj14eDhJGjsHS6Mkca3pvA7ZGKSXObIx+7/Rdtn57YDYyYp7h68KACDv6b3lkdqhs+VKM9ImRFq/CKB5Tye26YSdoLxDJNW9n/jT7NWfrlvqef7r87tb/FwUgGzuiMcbDZO25E5o9w75vhhhVrHivpNT2Z5WAYC1u145Wa+P9Q+WKmSN6kXR1yauTWq3bUEHCNE/nfMZAwAEMtldGhk9UR6pTpJ8cv5jUL7dMyr/BElt90zszhhg+a5/xjppbB0sVYjHapfmHwFBEbx2MHora7fHnyoAwPofvDlQrdX3DJUrmKQ2oYJO7JqPukA6e1j9wMBMbV4RAIBLY9fwubGhymjVLjsdg+NBNAfc/BBZfdeV2LtigFWPvj+cNOo7B8s12xsQtuFERcDsZP13h/+nAAB5N91brsSHRmv15tdRLyj/EI3RvVdq66oArn102Dg07lemERN1Q1iM0en9bPqR+b8AAKx+onKyNRD9ds47/Wx87OTV2HH4JD8nfJKsvpqx07uv1sR/BwBlpW1BULYSxgAAAABJRU5ErkJggg==',
          position: L.latLng(prtal.latlng.split(",")),
          guid: portal.guid,
          onSelected: window.plugin.plExtractor.onSearchResultSelected
        });
      });
    });
  };

  window.plugin.plExtractor.onSearchResultSelected = function(result, event) {
    // DELETE 
    //if(result.guid) { // portal
    var guid = result.guid;
    if(event.type === 'dblclick')
      zoomToAndShowPortal(guid, result.position);
    else if(window.portals[guid])
      renderPortalDetails(guid);
    else
      window.selectPortalByLatLng(result.position);
    // DELETE
    /*} else if(result.zoom) { // map
      map.setView(result.position, result.zoom);
    }*/
    return true; // prevent default behavior
  };

/***************************************************************************************************************************************************************/

  // Saved the new sort of the folders (in the localStorage)
  window.plugin.plExtractor.sortFolder = function(typeList) {
    var keyType = typeList.replace('prtl_', '');

    var newArr = {};
    $('#'+typeList+' li.portalFolder').each(function() {
        var idFold = $(this).attr('id');
      newArr[idFold] = window.plugin.plExtractor.prtlsObj[keyType][idFold];
    });
    window.plugin.plExtractor.prtlsObj[keyType] = newArr;
    window.plugin.plExtractor.saveStorage();

    window.runHooks('pluginPlExtractorEdit', {"target": "folder", "action": "sort"});
    console.log('PL EXTRACTOR: sorted folder');
  };

  // Saved the new sort of the bookmarks (in the localStorage)
  window.plugin.plExtractor.sortPortal = function(typeList) {
    var keyType = typeList.replace('prtl_', '');
    var list = window.plugin.plExtractor.prtlsObj[keyType];
    var newArr = {};

    $('#'+typeList+' li.portalFolder').each(function() {
      var idFold = $(this).attr('id');
      newArr[idFold] = window.plugin.plExtractor.prtlsObj[keyType][idFold];
      newArr[idFold].prtl = {};
    });

    $('#'+typeList+' li.prtl').each(function() {
      window.plugin.plExtractor.loadStorage();

      var idFold = $(this).parent().parent('li').attr('id');
      var id = $(this).attr('id');

      var list = window.plugin.plExtractor.prtlsObj[keyType];
      for(var idFoldersOrigin in list) {
        for(var idPrtl in list[idFoldersOrigin]['prtl']) {
          if(idPrtl === id) {
            newArr[idFold].prtl[id] = window.plugin.plExtractor.prtlsObj[keyType][idFoldersOrigin].prtl[id];
          }
        }
      }
    });
    window.plugin.plExtractor.prtlsObj[keyType] = newArr;
    window.plugin.plExtractor.saveStorage();
    window.runHooks('pluginPlExtractorEdit', {"target": "plExtractor", "action": "sort"});
    console.log('PL EXTRACTOR: sorted portal');
  };

  window.plugin.plExtractor.jquerySortableScript = function() {
    $(".portalList > ul").sortable({
      items:"li.portalFolder:not(.othersPlExtractor)",
      handle:".plExtractorAnchor",
      placeholder:"sortable-placeholder",
      helper:'clone', // fix accidental click in firefox
      forcePlaceholderSize:true,
      update:function(event, ui) {
        var typeList = $('#'+ui.item.context.id).parent().parent('.portalList').attr('id');
        window.plugin.plExtractor.sortFolder(typeList);
      }
    });

    $(".portalList ul li ul").sortable({
      items:"li.prtl",
      connectWith:".portalList ul ul",
      handle:".plExtractorLink",
      placeholder:"sortable-placeholder",
      helper:'clone', // fix accidental click in firefox
      forcePlaceholderSize:true,
      update:function(event, ui) {
        var typeList = $('#'+ui.item.context.id).parent().parent().parent().parent('.portalList').attr('id');
        window.plugin.plExtractor.sortPortal(typeList);
      }
    });
  };

/***************************************************************************************************************************************************************/
/** OPTIONS ****************************************************************************************************************************************************/
/***************************************************************************************************************************************************************/
  // Manual import, export and reset data
  window.plugin.plExtractor.manualOpt = function() {
    dialog({
      html: plugin.plExtractor.htmlSetbox,
      dialogClass: 'ui-dialog-prtlsSet',
      title: 'Portal List Extractor Options'
    });

    window.runHooks('pluginPlExtractorOpenOpt');
  };

  window.plugin.plExtractor.optAlert = function(message) {
      $('.ui-dialog-prtlsSet .ui-dialog-buttonset').prepend('<p class="prtls-alert" style="float:left;margin-top:4px;">'+message+'</p>');
      $('.prtls-alert').delay(2500).fadeOut();
  };

  window.plugin.plExtractor.optCopy = function() {
    if(typeof android !== 'undefined' && android && android.shareString) {
      return android.shareString(localStorage[window.plugin.plExtractor.KEY_STORAGE]);
    } else {
      dialog({
        html: '<p><a onclick="$(\'.ui-dialog-prtlsSet-copy textarea\').select();">Select all</a> and press CTRL+C to copy it.</p><textarea readonly>'+localStorage[window.plugin.plExtractor.KEY_STORAGE]+'</textarea>',
        dialogClass: 'ui-dialog-prtlsSet-copy',
        title: 'Portal List Extrator Export'
      });
    }
  };

  window.plugin.plExtractor.optExport = function() {
    if(typeof android !== 'undefined' && android && android.saveFile) {
      android.saveFile("IITC-plExtractor.json", "application/json", localStorage[window.plugin.plExtractor.KEY_STORAGE]);
    }
  };

  window.plugin.plExtractor.optPaste = function() {
    var promptAction = prompt('Press CTRL+V to paste it.', '');
    if(promptAction !== null && promptAction !== '') {
      try {
        JSON.parse(promptAction); // try to parse JSON first
        localStorage[window.plugin.plExtractor.KEY_STORAGE] = promptAction;
        window.plugin.plExtractor.refreshPrtls();
        window.runHooks('pluginPlExtractorEdit', {"target": "all", "action": "import"});
        console.log('PL EXTRACTOR: reset and imported portals');
        window.plugin.plExtractor.optAlert('Successful. ');
      } catch(e) {
        console.warn('PL EXTRACTOR: failed to import data: '+e);
        window.plugin.plExtractor.optAlert('<span style="color: #f88">Import failed </span>');
      }
    }
  };

  window.plugin.plExtractor.optImport = function() {
    if (window.requestFile === undefined) return;
    window.requestFile(function(filename, content) {
      try {
        JSON.parse(content); // try to parse JSON first
        localStorage[window.plugin.plExtractor.KEY_STORAGE] = content;
        window.plugin.plExtractor.refreshPrtls();
        window.runHooks('pluginPlExtractorEdit', {"target": "all", "action": "import"});
        console.log('PL EXTRACTOR: reset and imported portals');
        window.plugin.plExtractor.optAlert('Successful. ');
      } catch(e) {
        console.warn('PL EXTRACTOR: failed to import data: '+e);
        window.plugin.plExtractor.optAlert('<span style="color: #f88">Import failed </span>');
      }
    });
  };

  window.plugin.plExtractor.optReset = function() {
    var promptAction = confirm('All portals will be deleted. Are you sure?', '');
    if(promptAction) {
      delete localStorage[window.plugin.plExtractor.KEY_STORAGE];
      window.plugin.plExtractor.createStorage();
      window.plugin.plExtractor.loadStorage();
      window.plugin.plExtractor.refreshPrtls();
      window.runHooks('pluginPlExtractorEdit', {"target": "all", "action": "reset"});
      console.log('PL EXTRACTOR: reset all portals');
      window.plugin.plExtractor.optAlert('Successful. ');
    }
  };

  window.plugin.plExtractor.optBox = function(command) {
    if(!window.plugin.plExtractor.isAndroid()) {
      switch(command) {
        case 'save':
          var boxX = parseInt($('#plExtractorBox').css('top'));
          var boxY = parseInt($('#plExtractorBox').css('left'));
          window.plugin.plExtractor.statusBox.pos = {x:boxX, y:boxY};
          window.plugin.plExtractor.saveStorageBox();
          window.plugin.plExtractor.optAlert('Position acquired. ');
          break;
        case 'reset':
          $('#plExtractorBox').css({'top':100, 'left':100});
          window.plugin.plExtractor.optBox('save');
          break;
      }
    } else {
      window.plugin.plExtractor.optAlert('Only IITC desktop. ');
    }
  };

  window.plugin.plExtractor.dialogLoadListFolders = function(idBox, clickAction, showOthersF, scanType/*0 = maps&portals; 1 = maps; 2 = portals*/) {
    var list = JSON.parse(localStorage['plugin-plExtractor']);
    var listHTML = '';
    var foldHTML = '';
    var elemGenericFolder = '';

    // For each type and folder
    for(var type in list){
      // DELTE maps
      if(scanType === 0 /*|| (scanType === 1 && type === 'maps')*/ || (scanType === 2 && type === 'portals')){
        listHTML += '<h3>'+type+':</h3>';

        for(var idFolders in list[type]) {
          var label = list[type][idFolders]['label'];

          // Create a folder
          foldHTML = '<div class="portalFolder" id="'+idFolders+'" data-type="'+type+'" data-id="'+idFolders+'" onclick="'+clickAction+'(this)";return false;">'+label+'</div>';

          if(idFolders !== window.plugin.plExtractor.KEY_OTHER_PRTL) {
            listHTML += foldHTML;
          } else {
            if(showOthersF === true){
              elemGenericFolder = foldHTML;
            }
          }
        }
      }
      listHTML += elemGenericFolder;
      elemGenericFolder = '';
    }

    // Append all folders
    var r = '<div class="bookmarksDialog" id="'+idBox+'">'
      + listHTML
      + '</div>';

    return r;
  };

  window.plugin.plExtractor.renameFolder = function(elem){
    var type = $(elem).data('type');
    var idFold = $(elem).data('id');

    var promptAction = prompt('Insert a new name.', '');
    if(promptAction !== null && promptAction !== '') {
      try {
        var newName = window.plugin.plExtractor.escapeHtml(promptAction);

        window.plugin.plExtractor.prtlsObj[type][idFold].label = newName;
        $('#plExtractorDialogRenameF #'+idFold).text(newName);
        window.plugin.plExtractor.saveStorage();
        window.plugin.plExtractor.refreshPrtls();
        window.runHooks('pluginPlExtractorEdit', {"target": "all", "action": "import"});

        console.log('PL EXTRACTOR: renamed portal list folder');
        window.plugin.plExtractor.optAlert('Successful. ');
      } catch(e) {
        console.warn('PL EXTRACTOR: failed to rename folder: '+e);
        window.plugin.plExtractor.optAlert('<span style="color: #f88">Rename failed </span>');
        return;
      }
    }
  };

  window.plugin.plExtractor.optRenameF = function() {
    dialog({
      html: window.plugin.plExtractor.dialogLoadListFolders('plExtractorDialogRenameF', 'window.plugin.plExtractor.renameFolder', false, 0),
      dialogClass: 'ui-dialog-prtlsSet-copy',
      title: 'Portal List Extractor Rename Folder'
    });
  };

/***************************************************************************************************************************************************************/
/** AUTO DRAW **************************************************************************************************************************************************/
/***************************************************************************************************************************************************************/
  // DELETE tout l'auto draw
  /*window.plugin.plExtractor.dialogDrawer = function() {
    dialog({
      html:window.plugin.plExtractor.dialogLoadList,
      dialogClass:'ui-dialog-autodrawer',
      title:'Bookmarks - Auto Draw',
      buttons:{
        'DRAW': function() {
          window.plugin.plExtractor.draw(0);
        },
        'DRAW&VIEW': function() {
          window.plugin.plExtractor.draw(1);
        }
      }
    });
    window.plugin.plExtractor.autoDrawOnSelect();
  }

  window.plugin.plExtractor.draw = function(view) {
    var latlngs = [];
    var uuu = $('#prtlsAutoDrawer a.prtl.selected').each(function(i) {
      var tt = $(this).data('latlng');
      latlngs[i] = tt;
    });

    if(latlngs.length >= 2 && latlngs.length <= 3) {
      // TODO: add an API to draw-tools rather than assuming things about its internals

      var layer, layerType;
      if(latlngs.length == 2) {
        layer = L.geodesicPolyline(latlngs, window.plugin.drawTools.lineOptions);
        layerType = 'polyline';
      } else {
        layer = L.geodesicPolygon(latlngs, window.plugin.drawTools.polygonOptions);
        layerType = 'polygon';
      }

      map.fire('draw:created', {
        layer: layer,
        layerType: layerType
      });

      if($('#prtlClearSelection').prop('checked'))
        $('#prtlsAutoDrawer a.prtl.selected').removeClass('selected');

      if(window.plugin.plExtractor.isSmart) {
        window.show('map');
      }

      // Shown the layer if it is hidden
      if(!map.hasLayer(window.plugin.drawTools.drawnItems)) {
        map.addLayer(window.plugin.drawTools.drawnItems);
      }

      if(view) {
        map.fitBounds(layer.getBounds());
      }
    }
  }

  window.plugin.plExtractor.autoDrawOnSelect = function() {
    var latlngs = [];
    var uuu = $('#prtlsAutoDrawer a.prtl.selected').each(function(i) {
      var tt = $(this).data('latlng');
      latlngs[i] = tt;
    });

    var text = "You must select 2 or 3 portals!";
    var color = "red";

    function formatDistance(distance) {
      var text = digits(distance > 10000 ? (distance/1000).toFixed(2) + "km" : (Math.round(distance) + "m"));
      return distance >= 200000
        ? '<em title="Long distance link" class="help longdistance">'+text+'</em>'
        : text;
    }

    if(latlngs.length == 2) {
      var distance = L.latLng(latlngs[0]).distanceTo(latlngs[1]);
      text = 'Distance between portals: ' + formatDistance(distance);
      color = "";
    } else if(latlngs.length == 3) {
      var longdistance = false;
      var distances = latlngs.map(function(ll1, i, latlngs) {
        var ll2 = latlngs[(i+1)%3];
        return formatDistance(L.latLng(ll1).distanceTo(ll2));
      });
      text = 'Distances: ' + distances.join(", ");
      color = "";
    }

    $('#prtlsAutoDrawer p')
      .html(text)
      .css("color", color);
  }

  window.plugin.plExtractor.dialogLoadList = function() {
    var r = 'The "<a href="http://iitc.jonatkins.com/?page=desktop#plugin-draw-tools" target="_BLANK"><strong>Draw Tools</strong></a>" plugin is required.</span>';

    if(!window.plugin.plExtractor || !window.plugin.drawTools) {
      $('.ui-dialog-autodrawer .ui-dialog-buttonset .ui-button:not(:first)').hide();
    }
    else{
      var portalsList = JSON.parse(localStorage['plugin-plExtractor']);
      var element = '';
      var elementTemp = '';
      var elemGenericFolder = '';

      // For each folder
      var list = portalsList.portals;
      for(var idFolders in list) {
        var folders = list[idFolders];

        // Create a label and a anchor for the sortable
        var folderLabel = '<a class="folderLabel" onclick="$(this).siblings(\'div\').toggle();return false;">'+folders['label']+'</a>';

        // Create a folder
        elementTemp = '<div class="portalFolder" id="'+idFolders+'">'+folderLabel+'<div>';

        // For each bookmark
        var fold = folders['prtl'];
        for(var idPrtl in fold) {
          var prtl = fold[idPrtl];
          var label = prtl['label'];
          var latlng = prtl['latlng'];

          // Create the bookmark
          elementTemp += '<a class="prtl" id="'+idPrtl+'" onclick="$(this).toggleClass(\'selected\');return false" data-latlng="['+latlng+']">'+label+'</a>';
        }
        elementTemp += '</div></div>';

        if(idFolders !== window.plugin.plExtractor.KEY_OTHER_PRTL) {
          element += elementTemp;
        } else {
          elemGenericFolder += elementTemp;
        }
      }
      element += elemGenericFolder;

      // Append all folders and bookmarks
      r = '<div id="prtlsAutoDrawer">'
        + '<label style="margin-bottom: 9px; display: block;">'
        + '<input style="vertical-align: middle;" type="checkbox" id="prtlClearSelection" checked>'
        + ' Clear selection after drawing</label>'
        + '<p style="margin-bottom:9px;color:red">You must select 2 or 3 portals!</p>'
        + '<div onclick="window.plugin.plExtractor.autoDrawOnSelect();return false;">'
        + element
        + '</div>'
        + '</div>';
    }
    return r;
  }*/

/***************************************************************************************************************************************************************/
/** SYNC *******************************************************************************************************************************************************/
/***************************************************************************************************************************************************************/
  // Delay the syncing to group a few updates in a single request
  window.plugin.plExtractor.delaySync = function() {
    if(!window.plugin.plExtractor.enableSync) return;
    clearTimeout(plugin.plExtractor.delaySync.timer);
    window.plugin.plExtractor.delaySync.timer = setTimeout(function() {
        window.plugin.plExtractor.delaySync.timer = null;
        window.plugin.plExtractor.syncNow();
      }, window.plugin.plExtractor.SYNC_DELAY);
  };

  // Store the updateQueue in updatingQueue and upload
  window.plugin.plExtractor.syncNow = function() {
    if(!window.plugin.plExtractor.enableSync) return;
    $.extend(window.plugin.plExtractor.updatingQueue, window.plugin.plExtractor.updateQueue);
    window.plugin.plExtractor.updateQueue = {};
    window.plugin.plExtractor.storeLocal(window.plugin.plExtractor.UPDATING_QUEUE);
    window.plugin.plExtractor.storeLocal(window.plugin.plExtractor.UPDATE_QUEUE);

    window.plugin.sync.updateMap('plExtractor', window.plugin.plExtractor.KEY.field, Object.keys(window.plugin.plExtractor.updatingQueue));
  };

  // Call after IITC and all plugin loaded
  window.plugin.plExtractor.registerFieldForSyncing = function() {
    if(!window.plugin.sync) return;
    window.plugin.sync.registerMapForSync('plExtractor', window.plugin.plExtractor.KEY.field, window.plugin.plExtractor.syncCallback, window.plugin.plExtractor.syncInitialed);
  };

  // Call after local or remote change uploaded
  window.plugin.plExtractor.syncCallback = function(pluginName, fieldName, e, fullUpdated) {
    if(fieldName === window.plugin.plExtractor.KEY.field) {
      window.plugin.plExtractor.storeLocal(window.plugin.plExtractor.KEY);
      // All data is replaced if other client update the data during this client offline,
      if(fullUpdated) {
        window.plugin.plExtractor.refreshPrtls();
        return;
      }

      if(!e) return;
      if(e.isLocal) {
        // Update pushed successfully, remove it from updatingQueue
        delete window.plugin.plExtractor.updatingQueue[e.property];
      } else {
        // Remote update
        delete window.plugin.plExtractor.updateQueue[e.property];
        window.plugin.plExtractor.storeLocal(window.plugin.plExtractor.UPDATE_QUEUE);
        window.plugin.plExtractor.refreshPrtls();
        window.runHooks('pluginPlExtractorSyncEnd', {"target": "all", "action": "sync"});
        console.log('PL EXTRACTOR: synchronized all');
      }
    }
  };

  // syncing of the field is initialed, upload all queued update
  window.plugin.plExtractor.syncInitialed = function(pluginName, fieldName) {
    if(fieldName === window.plugin.plExtractor.KEY.field) {
      window.plugin.plExtractor.enableSync = true;
      if(Object.keys(window.plugin.plExtractor.updateQueue).length > 0) {
        window.plugin.plExtractor.delaySync();
      }
    }
  };

  window.plugin.plExtractor.storeLocal = function(mapping) {
    if(typeof(window.plugin.plExtractor[mapping.field]) !== 'undefined' && window.plugin.plExtractor[mapping.field] !== null) {
      localStorage[mapping.key] = JSON.stringify(window.plugin.plExtractor[mapping.field]);
    } else {
      localStorage.removeItem(mapping.key);
    }
  };

  window.plugin.plExtractor.loadLocal = function(mapping) {
    var objectJSON = localStorage[mapping.key];
    if(!objectJSON) return;
    window.plugin.plExtractor[mapping.field] = mapping.convertFunc
                            ? mapping.convertFunc(JSON.parse(objectJSON))
                            : JSON.parse(objectJSON);
  };

  window.plugin.plExtractor.syncPrtls = function() {
    window.plugin.plExtractor.loadLocal(window.plugin.plExtractor.KEY);

    window.plugin.plExtractor.updateQueue = window.plugin.plExtractor.prtlsObj;
    window.plugin.plExtractor.storeLocal(window.plugin.plExtractor.UPDATE_QUEUE);

    window.plugin.plExtractor.delaySync();
  };

/***************************************************************************************************************************************************************/
/** HIGHLIGHTER ************************************************************************************************************************************************/
/***************************************************************************************************************************************************************/
  window.plugin.plExtractor.highlight = function(data) {
    // DELETE console.log 
    //console.log(data.portal);
    var guid = data.portal.options.ent[0];
    if(window.plugin.plExtractor.findByGuid(guid)) {
      data.portal.setStyle({fillColor:'red'});
    }
  };

  window.plugin.plExtractor.highlightRefresh = function(data) {
    if(_current_highlighter === 'Portal List Extractor') {
      if(data.action === 'sync' || data.target === 'portal' || (data.target === 'folder' && data.action === 'remove') || (data.target === 'all' && data.action === 'import') || (data.target === 'all' && data.action === 'reset')) {
        window.resetHighlightedPortals();
      }
    }
  };

/***************************************************************************************************************************************************************/
/** BOOKMARKED PORTALS LAYER ***********************************************************************************************************************************/
/***************************************************************************************************************************************************************/
  window.plugin.plExtractor.addAllStars = function() {
    var list = window.plugin.plExtractor.prtlsObj.portals;

    for(var idFolders in list) {
      for(var idPrtls in list[idFolders]['prtl']) {
        var latlng = list[idFolders]['prtl'][idPrtls].latlng.split(",");
        var guid = list[idFolders]['prtl'][idPrtls].guid;
        var lbl = list[idFolders]['prtl'][idPrtls].label;
        window.plugin.plExtractor.addStar(guid, latlng, lbl);
      }
    }
  };

  window.plugin.plExtractor.resetAllStars = function() {
    for(guid in window.plugin.plExtractor.starLayers) {
      var starInLayer = window.plugin.plExtractor.starLayers[guid];
      window.plugin.plExtractor.starLayerGroup.removeLayer(starInLayer);
      delete window.plugin.plExtractor.starLayers[guid];
    }
    window.plugin.plExtractor.addAllStars();
  };

  window.plugin.plExtractor.addStar = function(guid, latlng, lbl) {
    var star = L.marker(latlng, {
      title: lbl,
      icon: L.icon({
        iconUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAoCAYAAACfKfiZAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAABhFJREFUeNqslm2MnFUVx3/3Pq/z8szLdrdddrbdl75ht9vSVUu6C6WmiSHNBIJ8gAQSQ/QDjYqr2GhQv5DstAsKFr5ojFZsiFSUNIGY8KVJUSMlkBQlBCuigsG03Zd2d2d2nplnnuuHOwvT7u7Ms9u9yWQmd27O+Z1zz/mfK5RSrGQV8mI/cD+wH9gK2EAF+AfwGvDCY6+o16LaE1EBCnmxF3gGuHVhL25DWxwMCXM+TBY/OX4OeOSxV9QbawJQyIvvAmOAkbBhbw9szEA2Dq4JCihXYXoe/jcDH07DZJGaa/L9Lz+vxm8IoJAXPwG+CXD7ZtjZCSlXR73UCkKYKcPHV+HiLBiS4weeVaOrAqhHfgzg7kHY0g6O2fq6lAI/gKmShrFNvtf/+NKZWBagkBf7gD8B8u5B2NYBlhG9WJWCSg3KAQgIXYvb7G+rv0QCKOSFBM4Dg1/YCkPd0SJfCqKmwJQCDPtvIG7h6/Nh45llbpIHgMHuDOzobOJcGGDGwUnrb2EA4tO/BRhi4Yc9iJV44HoTywGMAnx+E3hOkxANG5I5aBsIyWwBNwuGtQgCFeo9MzbaEqCQFzuAoVwacmmQogmANCHWAdltL5C9+cfEO8FOgemCkNdcBSgwnCGe27ajVQYOAWxfD8lF0QuQFjgZ8DZBZit4G8HJVDl06juYsXswE1ew02AlNCBCZyGs6XSY7qFGi0vd7jDAeu+66IUBtgeJLkj3QXwD2Gm9p+p1df8bpznR/1fM2ItIYwghoVqCMAAVaAjDGW6VgZ1JR4vNNc7dNmjfBd13wPrPXSLVO4bt3UYYtFOefOiTsw998AFhZZig/FOCeQirOv21KqgaGM5Aqwxs6vQgYTek3fag7TOwfghiHb+i5j/C3h/OLlsbX/mvDxw++aD4Y986fj3YhZF26+0u7Z5WAE4m1tB6hg3Jbn3fZvyXzPzrqxz8uYowP2LAnR/PYPS2QdqtZ9JwnFYAOGbD/VtJ3WpC/pOLb36Ne15VhbzwgBFgC9A+0sf5O55Vpxuc9wIvAXtuSoFrobvCsPWnBcC0EGSVAiGlTr+QMPXuE6+e+XPtrV+Ix4FvAcmFbtmd4zngdN35F4HfAG1daTiwBTKxessaLkhruhXAxWqNbBCCZdSVbfaj6qX/vPPbtz7id8BdAH3rYFcXDHSZlMoBhbwQwMLYlrfk4NYePbKlAKStW9OwL7YCeLtY4WY/AMsWULkKsx+++Yd3GQXuckzID0BPFlxbT6e/X8IDXgTuBTi4DXbeBHGrroSgxcn2QNpvtwI4N1PmvlIVkmEA5Wn+PaVSSYcfbG7Xg6lvHZgSUDWKPkjBl3rb9N6uLv3/tfND6FqyPTCsc00BPIeXywFPzVe0hlZqirjNwJ7cp08wU15bsP3tkMuAbegz5vXqYrpasu00IF9uKkTf+L163xC8PutDqBSWhGwMNmZhg7d4MppSD6z2hBavRc4R2nEyB1bidQ7+7P2W0zAT4/jVMpQqIITAMnR0TQfTcst0weuGeCdI63ikcdyZ4pQfcP7KfH2KsRrPdeGJdUCqH+zUeWqVU5EA9j2tVMxi9Mq8fu2u0jvYSUj3Q7ILhBzlwDMq6oOEfU+rs+WAExNF0M+2FWbBsCCRg/RmMOMnGDl6dsknRTMbtsGRqRITs/5Kg5fgtOn5EWufQNWOLPumaWZn17iarNZ49PIcVGsryIIZg1SvfrRI81FGjk2uCgAg6XDyyjxnpkoRC1KauuIzW8H2zlAtnWx6vBXA9jGlpODwRBG/WIlSeGntPL7BR4WH2f+UuiEAgD1PqgvlKoXLc1ALm2TBcHTaU71gOgVGjl5oZVtGrSvXYny6xHtaG1gMIQyId+joncx7BP54FLuRAQaOKj9UPHx5DvxALdPzmxceLw9z+5P+mgIAfPZH6myxsoQ2NPa8FT/B8NjZqDblSvXNMjgyWWzQhut7PgyOrMTeigEWaYMZj9zzawLQqA1zPpCI3vNrBrB9TClTctiQ+Hg9kOzyQbXs+TUDANj9hLqQSjgFYh1guAWGCxdWY8fkRpadOoYKdlOeGF+tif8PALm03MR6o2ueAAAAAElFTkSuQmCC',
        iconAnchor: [15,40],
        iconSize: [30,40]
      })
    });
    window.registerMarkerForOMS(star);
    star.on('spiderfiedclick', function() { renderPortalDetails(guid); });

    window.plugin.plExtractor.starLayers[guid] = star;
    star.addTo(window.plugin.plExtractor.starLayerGroup);
  };

  window.plugin.plExtractor.editStar = function(data) {
    if(data.target === 'portal') {
      if(data.action === 'add') {
        var guid = data.guid;
        var latlng = window.portals[guid].getLatLng();
        var lbl = window.portals[guid].options.data.title;
        var starInLayer = window.plugin.plExtractor.starLayers[data.guid];
        window.plugin.plExtractor.addStar(guid, latlng, lbl);
      }
      else if(data.action === 'remove') {
        var starInLayer = window.plugin.plExtractor.starLayers[data.guid];
        window.plugin.plExtractor.starLayerGroup.removeLayer(starInLayer);
        delete window.plugin.plExtractor.starLayers[data.guid];
      }
    }
    else if((data.target === 'all' && (data.action === 'import' || data.action === 'reset')) || (data.target === 'folder' && data.action === 'remove')) {
      window.plugin.plExtractor.resetAllStars();
    }
  };

/***************************************************************************************************************************************************************/

  window.plugin.plExtractor.setupCSS = function() {
    // TODO verfier le CSS
    $('<style>').prop('type', 'text/css').html('/* hide when printing */\n@media print {\n	#prtlsTrigger { display: none !important; }\n}\n\n\n#plExtractorBox *{\n	display:block;\n	padding:0;\n	margin:0;\n	width:auto;\n	height:auto;\n	font-family:Verdana,Geneva,sans-serif;\n	font-size:13px;\n	line-height:22px;\n	text-indent:0;\n	text-decoration:none;\n	-webkit-box-sizing:border-box;\n	-moz-box-sizing:border-box;\n	box-sizing:border-box;\n}\n\n#plExtractorBox{\n	display:block;\n	position:absolute !important;\n	z-index:4001;\n	top:100px;\n	left:100px;\n	width:231px;\n	height:auto;\n	overflow:hidden;\n}\n#plExtractorBox .addForm,\n#plExtractorBox #plExtractorTypeBar,\n#plExtractorBox h5{\n	height:28px;\n	overflow:hidden;\n	color:#fff;\n	font-size:14px;\n}\n#plExtractorBox #topBar{\n	height:15px !important;\n}\n#plExtractorBox #topBar *{\n	height: 14px !important;\n}\n#plExtractorBox #topBar *{\n	float:left !important;\n}\n#plExtractorBox .handle{\n	width:80%;\n	text-align:center;\n	color:#fff;\n	line-height:6px;\n	cursor:move;\n}\n#plExtractorBox #topBar .btn{\n	display:block;\n	width:10%;\n	cursor:pointer;\n	color:#20a8b1;\n\n	font-weight:bold;\n	text-align:center;\n	line-height:13px;\n	font-size:18px;\n}\n\n#plExtractorBox #topBar #plExtractorDel{\n	overflow:hidden;\n	text-indent:-999px;\n	background:#B42E2E;\n}\n\n#plExtractorBox #topBar #plExtractorMin:hover{\n	color:#ffce00;\n}\n#plExtractorBox #plExtractorTypeBar{\n	clear:both;\n}\n#plExtractorBox h5{\n	padding:4px 0 23px;\n	width:50%;\n	height:93px !important;\n	text-align:center;\n	color:#788;\n}\n#plExtractorBox h5.current{\n	cursor:default;\n	background:0;\n	color:#fff !important;\n}\n#plExtractorBox h5:hover{\n	color:#ffce00;\n	background:rgba(0,0,0,0);\n}\n#plExtractorBox #topBar,\n#plExtractorBox .addForm,\n#plExtractorBox #plExtractorTypeBar,\n#plExtractorBox .portalList li.bookmarksEmpty,\n#plExtractorBox .portalList li.prtl a,\n#plExtractorBox .portalList li.prtl:hover{\n	background-color:rgba(8,48,78,.85);\n}\n#plExtractorBox h5,\n#plExtractorBox .portalList li.prtl:hover .plExtractorLink,\n#plExtractorBox .addForm *{\n	background:rgba(0,0,0,.3);\n}\n#plExtractorBox .addForm *{\n	display:block;\n	float:left;\n	height:28px !important;\n}\n#plExtractorBox .addForm a{\n	cursor:pointer;\n	color:#20a8b1;\n	font-size:12px;\n	width:35%;\n	text-align:center;\n	line-height:20px;\n	padding:4px 0 23px;\n}\n#plExtractorBox .addForm a:hover{\n	background:#ffce00;\n	color:#000;\n	text-decoration:none;\n}\n#plExtractorBox .addForm input{\n	font-size:11px !important;\n	color:#ffce00;\n	height:28px;\n	padding:4px 8px 1px;\n	line-height:12px;\n	font-size:12px;\n}\n#plExtractorBox #prtl_portals .addForm input{\n	width:65%;\n}\n#plExtractorBox #prtl_maps .addForm input{\n	width:42%;\n}\n#plExtractorBox #prtl_maps .addForm a{\n	width:29%;\n}\n#plExtractorBox .addForm input:hover,\n#plExtractorBox .addForm input:focus{\n	outline:0;\n	background:rgba(0,0,0,.6);\n}\n#plExtractorBox .portalList > ul{\n	clear:both;\n	list-style-type:none;\n	color:#fff;\n	overflow:hidden;\n	overflow-y:auto;\n	max-height:580px;\n}\n#plExtractorBox .sortable-placeholder{\n	background:rgba(8,48,78,.55);\n	box-shadow:inset 1px 0 0 #20a8b1;\n}\n#plExtractorBox .ui-sortable-helper{\n	border-top-width:1px;\n}\n#plExtractorBox .portalList{\n	display:none;\n}\n#plExtractorBox .portalList.current{\n	display:block;\n}\n#plExtractorBox h5,\n#plExtractorBox .addForm *,\n#plExtractorBox ul li.prtl,\n#plExtractorBox ul li.prtl a{\n	height:22px;\n}\n#plExtractorBox h5,\n#plExtractorBox ul li.prtl a{\n	overflow:hidden;\n	cursor:pointer;\n	float:left;\n}\n#plExtractorBox ul .bookmarksEmpty{\n	text-indent:27px;\n	color:#eee;\n}\n#plExtractorBox ul .plExtractorRemoveFrom{\n	width:10%;\n	text-align:center;\n	color:#fff;\n}\n#plExtractorBox ul .plExtractorLink{\n	width:90%;\n	padding:0 10px 0 8px;\n	color:#ffce00;\n}\n#plExtractorBox ul .plExtractorLink.selected{\n	color:#03fe03;\n}\n#plExtractorBox ul .othersPlExtractor .plExtractorLink{\n	width:90%;\n}\n#plExtractorBox ul .plExtractorLink:hover{\n	color:#03fe03;\n}\n#plExtractorBox ul .plExtractorRemoveFrom:hover{\n	color:#fff;\n	background:#e22 !important;\n}\n\n/*---- UI border -----*/\n#plExtractorBox,\n#plExtractorBox *{\n	border-color:#20a8b1;\n	border-style:solid;\n	border-width:0;\n}\n#plExtractorBox #topBar,\n#plExtractorBox ul .portalFolder{\n	border-top-width:1px;\n}\n\n#plExtractorBox #topBar,\n#plExtractorBox #plExtractorTypeBar,\n#plExtractorBox .addForm,\n#plExtractorBox ul .portalFolder .folderLabel,\n#plExtractorBox ul li.prtl a {\n	border-bottom-width:1px;\n}\n#plExtractorBox ul .portalFolder{\n	border-right-width:1px;\n	border-left-width:1px;\n}\n#plExtractorBox #topBar *,\n#plExtractorBox #plExtractorTypeBar *,\n#plExtractorBox .addForm *,\n#plExtractorBox ul li.prtl{\n	border-left-width:1px;\n}\n#plExtractorBox #topBar,\n#plExtractorBox #plExtractorTypeBar,\n#plExtractorBox .addForm,\n#plExtractorBox ul .plExtractorRemoveFrom{\n	border-right-width:1px;\n}\n#plExtractorBox ul .portalFolder.othersPlExtractor li.prtl,\n#plExtractorBox ul .portalFolder .folderLabel .plExtractorRemoveFrom{\n	border-left-width:0;\n}\n#prtlsTrigger{\n	display:block;\n	position:absolute;\n	overflow:hidden;\n	top:0;\n	left:337px;\n	width:47px;\n	margin-top:-36px;\n	height:64px;\n	height:0;\n	cursor:pointer;\n	z-index:2999;\n	background-position:center bottom;\n	background-repeat:no-repeat;\n	transition:margin-top 100ms ease-in-out;\n	text-indent:-100%;\n	text-decoration:none;\n	text-align:center;\n}\n#prtlsTrigger:hover{\n	margin-top:0;\n}\n#sidebar #portaldetails h3.title{\n	width:auto;\n}\n.portal-list-portal span {\n	display:inline-block;\n	margin: -3px;\n	width:16px;\n	height:15px;\n	overflow:hidden;\n	background-repeat:no-repeat;\n	cursor:pointer;\n}\n#prtlsTrigger, .prtlsStar span, .portal-list-portal span {\n	background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC8AAABPCAYAAAB705z2AAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAACWVJREFUeNrUmmlQVFcWx3+Xxn3BBZdhcyEKUcRdI0YNo0bN6KQ0ygzolFFLZzST6ZTxw6TcwC2p1CSKNS41qWSMM+qY0mRMOWoSlygxKjGIaIxitBQEN8AFiMj25sPphu6mX9PLa4yn6lbD7Xvv+b9z/+fce85rpWkaT6sE8BRLgFIKVw0YCawCTDrf+09S1UhS1SpSlclbywcAi4HDmqaFPAFmLAYOk6pCfKHNSCBT07QXnwBDRgKZpCo73U73XdO0KCAeiLa0cbZfA2uA5UqpKsMdPlW5pRuzVqUcQPcD1gIvuKHmGJAE5BkE2mPdygZ4IrAFaOyBysPAaAOAe6VbWYAPA44CjTyYvBX4E/DIR+Be61aapgUAZ4EYC6cOAQeBq8BdwLqdVikHzEqpzZYH9wW4V7oxa5sBAoExlslZwAyl1DkHP7D9NxeYqpRKN8g97XRj1s45PFwd3Zi1Gt2BFqc7A8QrpR64UHQQSFRKFRgYW2p0Y9bq1Y1ZK3CM872BKS6Aa8BqYLzBwLHqdgG8RrcjcCstkqz00Gku7wAu5tXf1pHky3z1NN8qA0lVtejNDg/yxR+8W1WZIDweuoyH3MOQ8yVUV3q31rh/oefEgYabIzgWopKgZaj8H5UI4b+G7P/A7dMGW96VFJ6HVl2gcav6V2oZKqCDY+t+17wT9DND0Y9waRs8vN4A4O9mQnEOtO4qLbB53TFN2kCPqRDyvNDFKhUl8ONWiJoOTYKkr92z8NxKyD8Gl3fB4/t+BA9QViStJA9ad4Og7qACwNQYukyA7hPB1NR+zuVdkHtIHuDO9xA+WnYFQCkIHQWdh8LVvXB9P1SV+wm8VX6+bWl3IGYO9PkjNG1vP+b+T3BhCxTb0KKqHK7tFwr2mgVteki/qansWHg8ZO+EmyctYd0f4AE6Pwej1sqnrRRfh4vboeiC/tziXDi1Atr1gugk8SUQA8QugIgXxR/u/2Qw+FYRMPwdiPq9ff5Skg/n/wEPrrhvgKIL8O0S6DQIomfU7l6bZ2DoMtmB7J1QVugj+CZtYMgS6Pu6cNwq1ZVw1AxZmyQStY2S5oncPg23v4eI0fIQyiSG+dUw6DhQfOHqXtfHibYO/UPqUQE0C7bvyz8OR+ZDgf0FkLbR0C4aGrWsq6X/G3D+A6go1dnZcOj1KrTpad//+L4Y0KtDyhb4rXRIexPyv3E+9t5FqCwVTjfrYP9dx4EQ1xXO/t05p4tz4dRKaNtTdqF1t9qd96nodO8i7PkN7ByqD9wWxJ0M+XSUpu2Fgt0m6eX9cC8bTiyDM2slqvnE+QNJcGmHZ1x+dBcKMqG6Qs4ExztPzwQ5rM5thvKHzte4kyEtdCTEzPXS8p4Cr+HqAyg4qx/2gvtA3GqhmCvJO/aEapUVpZYHuKwfyQb9FZ6ZIie2X09Yd6VFiFzOWneF5h2hcZCLWKcgcrLQKGsjlN17AuA79INnZ0Lky7WRwhNpGw1xayBrs+xWg4Dv0B+Gvw1dxvlugEYtYeAiuLYPsj8BrcpP4JVJjvIhS7zmq650fUlO67MbJHIZ6rCBzWDibgGv/OTvQZHQa6bBllcmmLATuk/yb2Z9/UvI3mEw+KFL/Qu86rHcgW6dMthhOw2GIUvdG1tWKFlUVYXcd4L71j+nJA8y10Npvh+izahU5xwvyILrByD/WwlzpTfFgrZiridDunkCfvgIqsr8EOcjxsg921ZyDsKJJW5vsVOprpCc4PZ3fjxh+75ur/DInyWD8kWKc2HfVLk9hgzXv2n6BL5pO+g6Qf6ufASfT5LKgE/R5As4ML021WsRUvcG6obUH6jDx0BAI9CqYe9k34Br1XAyGfa8ZJ+jPrwmO2q45cNGyWfaIrGYt1JWKNZ2tkZpvhS3giINtnyHfnLfOLPOe+C30mH7ANcPX5wjO2Ms59vDvgSPC0J2smtE/VWxkjxprcINsnyvWXD6HVnUp5PTzXJeqYMex/DskeXHfiQFpbIiuPo5fpeSPCl3hI2S2maLEB9pExQJk/ZIwfToG/VXEHyRjgPh+Xcls/KZNqU37ReelgbTvpEE2kgJ7iPrTkuzB15e7AP4j3tC+mo5nKwSMhwSM6Q4iq+/t1GyTmKG5ZS1nuLlEuHSFvkAvqJE7i9boy1lEEvECQiE+A0w+5ok0N5I5GSZH79B1pNTTPT8L0E+K382IJMqzpEC1M44uHXSpsYYARM/haQzEBbvHuiweBk/8VOZX3MWnJT1DyS5LpV7nQbaKijOsT/IXjkMCcedv5OyjvndCRnXoZ9rw5QV+QG87dZujRZKVZTYxOU4mH4WBiyUXNd6sRuyWKxt+1JCj5JW8HqlQEMqZpWPxJk/7imJhO3RPuI9mHVVXmPOvAzDVtk4Y6Wkes6Cga2BHj/wI3jbcHpwDuwYBDe+ru1v3hnGbxfL11yFD8C2vnBonn0Y1gsWfgdvlbtnYHc87J1St8BadAH+O0Gam87YsOCtcuUz+HdvufdrVXKT3NZXrO6J6L1F8Sv49jHw273y7lWZpBT48j7p99SnGgx8YHMpls44BxFjHRL4sdIft8b5W3S9Gk6DgB+wEF4rhcFvuR43+C0ZN2ChG5WFcj+D75EArxyR0OjUiTOd9494T+b1SHABvv6fuHhXJe40CPrMh96z9Z02axPkfCWUiZ1f9w4U9oK0H8bBuU11f87iRkroGfiWodB/oettPzRPDqGa4tRX0mLmwmgntZ7es6Vl/E3yZGvW5kYF2j3aqACpU865oQ/8xDJYb7IFvgWzppBfqkr/epOMc+o3i2T9IUtFX0CgAeBj5sJfqmDYCuffZ7wPH4ZB+krbrU7BrM2y1ClnASk1VEhfKeMz3ne+3rAVoi9mXv02dfn6/spn+vf1SzuEq3lptr0pmLVkXW2pKhlYXvN/6AjxnahE5+Pzj9snKbKGm+CdyY2vxRkvf2Lbu6XG0u5Iqvon8Kpd1IqdLw5c/1wvQ2Xam3J/sQee4hFwRyqBrLc7XtY3tGIG8N3bsKGFI09TMGvKJU1cP0CyJQlOsfOfDS1EnztxxCVtsjYKRQrPe08R92gEYE+l9jFCpdgFbtIma1Pt39f2w5HXHIGnGA681mj2VCo8L/rdtry+bPELaP1dqI1K8RtlB6zGPbLAI/ApXvPaaCp54LC+OaQ/qOQGbRqOIj7uQkAda8vT/rJEZxeslk8Bkt06YX8Zu5AMLP//AExw7AVdBCnDAAAAAElFTkSuQmCC);\n}\n.prtlsStar span{\n	display:inline-block;\n	float:left;\n	margin:3px 1px 0 4px;\n	width:16px;\n	height:15px;\n	overflow:hidden;\n	background-repeat:no-repeat;\n}\n.prtlsStar span, .prtlsStar.favorite:focus span{\n	background-position:left top;\n}\n.prtlsStar:focus span, .prtlsStar.favorite span, .portal-list-portal.favorite span{\n	background-position:right top;\n}\n#plExtractorBox .portalList .portalFolder{\n	overflow:hidden;\n	margin-top:-1px;\n	height:auto;\n	background:rgba(8,58,78,.7);\n}\n#plExtractorBox .portalList ul li.sortable-placeholder{\n	box-shadow:inset -1px 0 0 #20a8b1,inset 1px 0 0 #20a8b1,0 -1px 0 #20a8b1;\n	background:rgba(8,58,78,.9);\n}\n#plExtractorBox .portalList .prtl.ui-sortable-helper{\n	border-right-width:1px;\n	border-left-width:1px !important;\n}\n#plExtractorBox .portalList ul li ul li.sortable-placeholder{\n	height:23px;\n	box-shadow:inset 0 -1px 0 #20a8b1,inset 1px 0 0 #20a8b1;\n}\n\n#plExtractorBox .portalList ul li.portalFolder.ui-sortable-helper,\n#plExtractorBox .portalList ul li.othersPlExtractor ul li.sortable-placeholder{\n	box-shadow:inset 0 -1px 0 #20a8b1;\n}\n\n#plExtractorBox #topBar #plExtractorDel,\n#plExtractorBox .portalList .portalFolder .folderLabel:hover .plExtractorRemoveFrom,\n#plExtractorBox .portalList .portalFolder .folderLabel:hover .plExtractorAnchor{\n	border-bottom-width:1px;\n}\n\n/*---------*/\n#plExtractorBox .portalList .portalFolder .folderLabel .plExtractorAnchor span,\n#plExtractorBox .portalList .portalFolder .folderLabel > span,\n#plExtractorBox .portalList .portalFolder .folderLabel > span > span,\n#plExtractorBox .portalList .triangle{\n	width:0;\n	height:0;\n}\n#plExtractorBox .portalList .portalFolder .folderLabel{\n	overflow:visible;\n	height:25px;\n	cursor:pointer;\n	background:#069;\n	text-indent:0;\n}\n#plExtractorBox .portalList .portalFolder .folderLabel > *{\n	height:25px;\n	float:left;\n}\n#plExtractorBox .portalList .portalFolder .folderLabel .plExtractorAnchor{\n	line-height:25px;\n	color:#fff;\n	width:90%;\n}\n#plExtractorBox .portalList .portalFolder .folderLabel .plExtractorAnchor span{\n	float:left;\n	border-width:5px 0 5px 7px;\n	border-color:transparent transparent transparent white;\n	margin:7px 7px 0 6px;\n}\n#plExtractorBox .portalList .portalFolder.open .folderLabel .plExtractorAnchor span{\n	margin:9px 5px 0 5px;\n	border-width:7px 5px 0 5px;\n	border-color:white transparent transparent transparent;\n}\n#plExtractorBox .portalList .portalFolder .folderLabel > span,\n#plExtractorBox .portalList .portalFolder .folderLabel > span > span{\n	display:none;\n	border-width:0 12px 10px 0;\n	border-color:transparent #20a8b1 transparent transparent;\n	margin:-20px 0 0;\n	position:relative;\n	top:21px;\n	left:219px;\n}\n#plExtractorBox .portalList .portalFolder .folderLabel > span > span{\n	top:18px;\n	left:0;\n	border-width:0 10px 9px 0;\n	border-color:transparent #069 transparent transparent;\n}\n#plExtractorBox .portalList .portalFolder.open .folderLabel > span,\n#plExtractorBox .portalList .portalFolder.open .folderLabel > span > span{\n	display:block;\n	display:none;\n}\n#plExtractorBox .portalList .portalFolder.open .folderLabel:hover > span > span{\n	border-color:transparent #036 transparent transparent;\n}\n#plExtractorBox .portalList .portalFolder .folderLabel:hover .plExtractorAnchor{\n	background:#036;\n}\n#plExtractorBox .portalList .portalFolder ul{\n	display:none;\n	margin-left:10%;\n}\n#plExtractorBox .portalList .portalFolder.open ul{\n	display:block;\n	min-height:22px;\n}\n#plExtractorBox .portalFolder.othersPlExtractor ul{\n	margin-left:0;\n}\n\n/*---- Width for deleteMode -----*/\n#plExtractorBox .plExtractorRemoveFrom{\n	display:none !important;\n}\n#plExtractorBox.deleteMode .plExtractorRemoveFrom{\n	display:block !important;\n}\n\n#plExtractorBox .portalList .portalFolder .folderLabel .plExtractorAnchor,\n#plExtractorBox ul .plExtractorLink,\n#plExtractorBox ul .othersPlExtractor .plExtractorLink{\n	width:100% !important;\n}\n\n#plExtractorBox.deleteMode .portalList .portalFolder .folderLabel .plExtractorAnchor,\n#plExtractorBox.deleteMode ul .plExtractorLink,\n#plExtractorBox.deleteMode ul .othersPlExtractor .plExtractorLink{\n	width:90% !important;\n}\n\n/**********************************************\n	MOBILE\n**********************************************/\n#plExtractorBox.mobile{\n	position:absolute !important;\n	width: 100% !important;\n	height: 100% !important;\n	top: 0 !important;\n	left: 0 !important;\n	margin: 0 !important;\n	padding: 0 !important;\n	border: 0 !important;\n	background: transparent !important;\n	overflow:auto !important;\n}\n#plExtractorBox.mobile .portalList ul,\n#plExtractorBox.mobile .portalList ul li,\n#plExtractorBox.mobile .portalList.current,\n#plExtractorBox.mobile .portalList li.portalFolder.open ul{\n	width:100% !important;\n	display:block !important;\n}\n#plExtractorBox.mobile *{\n	box-shadow:none !important;\n	border-width:0 !important;\n}\n#plExtractorBox.mobile #topBar #plExtractorMin,\n#plExtractorBox.mobile #topBar .handle{\n	display:none !important;\n}\n\n#plExtractorBox.mobile #plExtractorTypeBar h5{\n	cursor:pointer;\n	text-align:center;\n	float:left;\n	width:50%;\n	height:auto !important;\n	padding:7px 0;\n}\n#plExtractorBox.mobile #plExtractorTypeBar h5.current{\n	cursor:default;\n	color:#fff;\n}\n#plExtractorBox.mobile #plExtractorTypeBar,\n#plExtractorBox.mobile .portalList .addForm{\n	border-bottom:1px solid #20a8b1 !important;\n}\n#plExtractorBox.mobile .portalList ul li ul li.prtl,\n#plExtractorBox.mobile .portalList li.portalFolder .folderLabel{\n	height:36px !important;\n	clear:both;\n}\n#plExtractorBox.mobile .portalList li.portalFolder .folderLabel a,\n#plExtractorBox.mobile .portalList ul li ul li.prtl a{\n	background:none;\n	padding:7px 0;\n	height:auto;\n	box-shadow:inset 0 1px 0 #20a8b1 !important;\n}\n#plExtractorBox.mobile .portalList li.portalFolder a.plExtractorRemoveFrom,\n#plExtractorBox.mobile .portalList li.prtl a.plExtractorRemoveFrom{\n	box-shadow:inset 0 1px 0 #20a8b1,inset -1px 0 0 #20a8b1 !important;\n	width:10%;\n	background:none !important;\n}\n#plExtractorBox.mobile .portalList li.portalFolder a.plExtractorAnchor,\n#plExtractorBox.mobile .portalList li.prtl a.plExtractorLink{\n	text-indent:10px;\n	height:36px;\n	line-height:24px;\n	overflow:hidden;\n}\n#plExtractorBox.mobile .portalList ul li.portalFolder ul{\n	margin-left:0 !important;\n}\n#plExtractorBox.mobile .portalList > ul{\n	border-bottom:1px solid #20a8b1 !important;\n}\n\n#plExtractorBox.mobile .portalList .portalFolder.othersPlExtractor  ul{\n	border-top:5px solid #20a8b1 !important;\n}\n#plExtractorBox.mobile .portalList li.portalFolder,\n#plExtractorBox.mobile .portalList li.prtl{\n	box-shadow:inset 0 1px 0 #20a8b1, 1px 0 0 #20a8b1, -1px 1px 0 #20a8b1 !important;\n}\n#plExtractorBox.mobile .portalList > ul{\n	max-height:none;\n/*	width:85% !important;*/\n}\n#plExtractorBox.mobile .portalList li.portalFolder .folderLabel{\n	box-shadow:0 1px 0 #20a8b1 !important;\n}\n#plExtractorBox.mobile .portalList ul li.portalFolder ul{\n	width:90% !important;\n	margin-left:10% !important;\n}\n#plExtractorBox.mobile .portalList ul li.portalFolder.othersPlExtractor ul{\n	width:100% !important;\n	margin-left:0% !important;\n}\n#plExtractorBox.mobile{\n	margin-bottom:5px !important;\n}\n#plExtractorBox.mobile #plExtractorTypeBar{\n	height:auto;\n}\n#plExtractorBox.mobile .addForm,\n#plExtractorBox.mobile .addForm *{\n	height:35px !important;\n	padding:0;\n}\n#plExtractorBox.mobile .addForm a{\n	line-height:37px;\n}\n\n#plExtractorBox.mobile .addForm a{\n/*	width:25% !important;*/\n}\n#plExtractorBox.mobile .addForm input{\n/*	width:50% !important;*/\n	text-indent:10px;\n}\n#plExtractorBox.mobile #prtl_portals .addForm input{\n/*	width:75% !important;*/\n}\n#plExtractorBox.mobile #plExtractorTypeBar h5,\n#plExtractorBox.mobile .portalList .addForm a{\n	box-shadow:-1px 0 0 #20a8b1 !important;\n}\n#plExtractorBox.mobile .portalList li.portalFolder ul{\n	display:none !important;\n	min-height:37px !important;\n}\n#updatestatus .prtlsStar{\n	float:left;\n	margin:-19px 0 0 -5px;\n	padding:0 3px 1px 4px;\n	background:#262c32;\n}\n#plExtractorBox.mobile .portalList .portalFolder .folderLabel .plExtractorAnchor span,\n#plExtractorBox.mobile .portalList .portalFolder .folderLabel > span,\n#plExtractorBox.mobile .portalList .portalFolder .folderLabel > span > span,\n#plExtractorBox.mobile .portalList .triangle{\n	width:0 !important;\n	height:0 !important;\n}\n#plExtractorBox.mobile .portalList .portalFolder .folderLabel .plExtractorAnchor span{\n	float:left !important;\n	border-width:5px 0 5px 7px !important;\n	border-color:transparent transparent transparent white !important;\n	margin:7px 3px 0 13px !important;\n}\n#plExtractorBox.mobile .portalList .portalFolder.open .folderLabel .plExtractorAnchor span{\n	margin:9px 1px 0 12px !important;\n	border-width:7px 5px 0 5px !important;\n	border-color:white transparent transparent transparent !important;\n}\n#plExtractorBox.mobile .portalList .portalFolder .folderLabel > span,\n#plExtractorBox.mobile .portalList .portalFolder .folderLabel > span > span{\n	display:none !important;\n	border-width:0 12px 10px 0 !important;\n	border-color:transparent #20a8b1 transparent transparent !important;\n	margin:-20px 0 0 100% !important;\n	position:relative !important;\n	top:21px !important;\n	left:-10px !important;\n}\n#plExtractorBox.mobile .portalList .portalFolder .folderLabel > span > span{\n	top:18px !important;\n	left:0 !important;\n	border-width:0 10px 9px 0 !important;\n	border-color:transparent #069 transparent transparent !important;\n}\n#plExtractorBox.mobile .portalList .portalFolder.open .folderLabel > span,\n#plExtractorBox.mobile .portalList .portalFolder.open .folderLabel > span > span{\n	display:block !important;\n}\n\n/**********************************************\n	DIALOG BOX\n**********************************************/\n/*---- Auto Drawer -----*/\n#prtlsAutoDrawer,\n#prtlsAutoDrawer p,\n#prtlsAutoDrawer a{\n	display:block;\n	padding:0;\n	margin:0;\n}\n#prtlsAutoDrawer .portalFolder{\n	margin-bottom:4px;\n	border:1px solid #20a8b1;\n}\n#prtlsAutoDrawer .folderLabel{\n	background:#069;\n	padding:4px 0;\n	color:#fff;\n}\n#prtlsAutoDrawer .portalFolder div{\n	border-top:1px solid #20a8b1;\n	padding:6px 0;\n	background:rgba(0,0,0,0.3);\n}\n#prtlsAutoDrawer .portalFolder#idOthers .folderLabel{\n	display:none;\n}\n#prtlsAutoDrawer .portalFolder#idOthers div{\n	display:block;\n	border-top:none;\n}\n#prtlsAutoDrawer a{\n	text-indent:10px;\n	padding:2px 0;\n}\n#prtlsAutoDrawer .longdistance {\n	color: #FFCC00;\n	font-weight: bold;\n	border-bottom: 1px dashed currentColor;\n}\n#prtlsAutoDrawer .portalFolder div {\n	display:none;\n}\n#prtlsAutoDrawer a.prtl.selected{\n	color:#03dc03;\n}\n\n/*---- Options panel -----*/\n#prtlsSetbox a{\n	display:block;\n	color:#ffce00;\n	border:1px solid #ffce00;\n	padding:3px 0;\n	margin:10px auto;\n	width:80%;\n	text-align:center;\n	background:rgba(8,48,78,.9);\n}\n#prtlsSetbox a.disabled,\n#prtlsSetbox a.disabled:hover{\n	color:#666;\n	border-color:#666;\n	text-decoration:none;\n}\n/*---- Opt panel - copy -----*/\n.ui-dialog-prtlsSet-copy textarea{\n	width:96%;\n	height:120px;\n	resize:vertical;\n}\n\n\n/*--- Other Opt css ---*/\n#plExtractorBox.mobile a.plExtractorMoveIn{\n	display:none !important;\n}\n\n#plExtractorBox.mobile .portalList ul li ul li.prtl a.plExtractorMoveIn{\n	background:none !important;\n	text-align:center;\n	color:#fff;\n	box-shadow: inset 0 1px 0 #20A8B1,inset -1px 0 0 #20A8B1 !important;\n	width:10%;\n}\n\n#plExtractorBox.mobile.moveMode a.plExtractorMoveIn{\n	display:block !important;\n}\n\n#plExtractorBox.moveMode ul .plExtractorLink,\n#plExtractorBox.moveMode ul .othersPlExtractor .plExtractorLink{\n	width:90% !important;\n}\n.bookmarksDialog h3{\n	text-transform:capitalize;margin-top:10px;\n}\n.bookmarksDialog .portalFolder{\n	margin-bottom:4px;\n	border:1px solid #20a8b1;\n	background:#069;\n	padding:4px 10px;\n	color:#fff;\n	cursor:pointer;\n}\n.bookmarksDialog .portalFolder:hover{\n	text-decoration:underline;\n}\n\n#plExtractorBox.mobile #topBar .btn{\n	width:100%;height:45px !important;\n	font-size:13px;\n	color:#fff;\n	font-weight:normal;\n	padding-top:17px;\n	text-indent:0 !important;\n}\n#plExtractorBox.mobile .btn{\n	width:50% !important;\n	background:#222;\n}\n#plExtractorBox.mobile .btn.left{\n	border-right:1px solid #20a8b1 !important;\n}\n#plExtractorBox.mobile .btn#plExtractorMove{\n	background:#B42E2E;\n}\n#prtlsSetbox{\n	text-align:center;\n}\n').appendTo('head');
  };
	
  window.plugin.plExtractor.setupPortalsList = function() {
    function onPortalChanged(data) {
       // DELETE console.log
       //console.log(data, data.target, data.guid);


      if(data.target === "portal" && data.guid) {
        if(plugin.plExtractor.findByGuid(data.guid))
          $('[data-list-portal="'+data.guid+'"]').addClass("favorite");
        else
          $('[data-list-portal="'+data.guid+'"]').removeClass("favorite");
      } else {
        $('[data-list-portal]').each(function(i, element) {
          var guid = element.getAttribute("data-list-portal");
          if(plugin.plExtractor.findByGuid(guid))
            $(element).addClass("favorite");
          else
            $(element).removeClass("favorite");
        });
      }
    }

    window.addHook('pluginPlExtractorEdit', onPortalChanged);
    window.addHook('pluginPlExtractorSyncEnd', onPortalChanged);

    window.plugin.portalslist.fields.unshift({ // insert at first column
      title: "",
      value: function(portal) { return portal.options.guid; }, // we store the guid, but implement a custom comparator so the list does sort properly without closing and reopening the dialog
      sort: function(guidA, guidB) {
        var infoA = plugin.plExtractor.findByGuid(guidA);
        var infoB = plugin.plExtractor.findByGuid(guidB);
        if(infoA && !infoB) return 1;
        if(infoB && !infoA) return -1;
        return 0;
      },
      format: function(cell, portal, guid) {
        $(cell)
          .addClass("portal-list-portal")
          .attr("data-list-portal", guid);

        // for some reason, jQuery removes event listeners when the list is sorted. Therefore we use DOM's addEventListener
        $('<span>').appendTo(cell)[0].addEventListener("click", function() {
          if(window.plugin.plExtractor.findByGuid(guid)) {
            window.plugin.plExtractor.switchStarPortal(guid);
          } else {
            var ll = portal.getLatLng();
            plugin.plExtractor.addPortalExtract(guid, ll.lat+','+ll.lng, portal.options.data.title, portal.options.data['image']);
          }
        }, false);

        if(plugin.plExtractor.findByGuid(guid))
          cell.className += " favorite";
      }
    });
  };

  window.plugin.plExtractor.setupContent = function() {
    plugin.plExtractor.htmlBoxTrigger = '<a id="prtlsTrigger" class="open" onclick="window.plugin.plExtractor.switchStatusPrtlsBox(\'switch\');return false;" accesskey="v" title="[v]">[-] Portals</a>';
    plugin.plExtractor.htmlPrtlsBox = '<div id="plExtractorBox">'
                          +'<div id="topBar">'
                            +'<a id="plExtractorMin" class="btn" onclick="window.plugin.plExtractor.switchStatusPrtlsBox(0);return false;" title="Minimize">-</a>'
                            +'<div class="handle">...</div>'
                            +'<a id="plExtractorDel" class="btn" onclick="window.plugin.plExtractor.deleteMode();return false;" title="Show/Hide \'X\' button">Show/Hide "X" button</a>'
                          +'</div>'
                          +'<div id="plExtractorTypeBar">'
                            +'<h5 class="prtl_portals" onclick="window.plugin.plExtractor.switchPagePrtlsBox(this, 1);return false">Portals</h5>'
                            +'<div style="clear:both !important;"></div>'
                          +'</div>'
                          +'<div id="prtl_maps" class="portalList current">'
                            +'<div class="addForm">'
                              +'<input placeholder="Insert label" />'
                              +'<a class="newFolder" onclick="window.plugin.plExtractor.addElement(this, \'folder\');return false;">+ Folder</a>'
                            +'</div>'
                          +'</div>'
                          +'<div id="prtl_portals" class="portalList">'
                            +'<div class="addForm">'
                              +'<input placeholder="Insert label" />'
                              +'<a class="newFolder" onclick="window.plugin.plExtractor.addElement(this, \'folder\');return false;">+ Folder</a>'
                            +'</div>'
                          +'</div>'
                          +'<div style="border-bottom-width:1px;"></div>'
                        +'</div>';

    plugin.plExtractor.htmlDisabledMessage = '<div title="Your browser do not support localStorage">Plugin Portal List Extractor disabled*.</div>';
    plugin.plExtractor.htmlStar = '<a class="prtlsStar" accesskey="b" onclick="window.plugin.plExtractor.switchStarPortal();return false;" title="Save this portal in your portal list [b]"><span></span></a>';
    // DELETE
    //plugin.plExtractor.htmlCalldrawBox = '<a onclick="window.plugin.plExtractor.dialogDrawer();return false;" accesskey="q" title="Draw lines/triangles between bookmarked portals [q]">Auto draw</a>';
    plugin.plExtractor.htmlCallSetBox = '<a onclick="window.plugin.plExtractor.manualOpt();return false;">plExtractor Opt</a>';
    plugin.plExtractor.htmlMoveBtn = '<a id="plExtractorMove" class="btn" onclick="window.plugin.plExtractor.moveMode();return false;">Show/Hide "Move" button</a>';

    var actions = '';
    actions += '<a onclick="window.plugin.plExtractor.optReset();return false;">Reset portal list</a>';
    actions += '<a onclick="window.plugin.plExtractor.optCopy();return false;">Copy portal list</a>';
    actions += '<a onclick="window.plugin.plExtractor.optPaste();return false;">Paste portal list</a>';

    if(plugin.plExtractor.isAndroid()) {
      actions += '<a onclick="window.plugin.plExtractor.optImport();return false;">Import portal list</a>';
      actions += '<a onclick="window.plugin.plExtractor.optExport();return false;">Export portal list</a>';
    }
    actions += '<a onclick="window.plugin.plExtractor.optRenameF();return false;">Rename Folder</a>';
    if(!plugin.plExtractor.isAndroid()) {
      actions += '<a onclick="window.plugin.plExtractor.optBox(\'save\');return false;">Save box position</a>';
      actions += '<a onclick="window.plugin.plExtractor.optBox(\'reset\');return false;">Reset box position</a>';
    }
    plugin.plExtractor.htmlSetbox = '<div id="prtlsSetbox">' + actions + '</div>';
  };

/***************************************************************************************************************************************************************/

  var setup = function() {
    window.plugin.plExtractor.isSmart = window.isSmartphone();

    // Fired when a portals/folder is removed, added or sorted, also when a folder is opened/closed.
    if($.inArray('pluginPlExtractorEdit', window.VALID_HOOKS) < 0) { window.VALID_HOOKS.push('pluginPlExtractorEdit'); }
    // Fired when the "Portal List Extractor Options" panel is opened (you can add new options);
    if($.inArray('pluginPlExtractorOpenOpt', window.VALID_HOOKS) < 0) { window.VALID_HOOKS.push('pluginPlExtractorOpenOpt'); }
    // Fired when the sync is finished;
    if($.inArray('pluginPlExtractorSyncEnd', window.VALID_HOOKS) < 0) { window.VALID_HOOKS.push('pluginPlExtractorSyncEnd'); }

    // If the storage not exists or is a old version
    window.plugin.plExtractor.createStorage();
    window.plugin.plExtractor.upgradeToNewStorage();

    // Load data from localStorage
    window.plugin.plExtractor.loadStorage();
    window.plugin.plExtractor.loadStorageBox();
    window.plugin.plExtractor.setupContent();
    window.plugin.plExtractor.setupCSS();

    if(!window.plugin.plExtractor.isSmart) {
      $('body').append(window.plugin.plExtractor.htmlBoxTrigger + window.plugin.plExtractor.htmlPrtlsBox);
      $('#plExtractorBox').draggable({ handle:'.handle', containment:'window' });
      $("#plExtractorBox #plExtractorMin , #plExtractorBox ul li, #plExtractorBox ul li a, #plExtractorBox ul li a span, #plExtractorBox h5, #plExtractorBox .addForm a").disableSelection();
      $('#plExtractorBox').css({'top':window.plugin.plExtractor.statusBox.pos.x, 'left':window.plugin.plExtractor.statusBox.pos.y});
    }else{
      $('body').append(window.plugin.plExtractor.htmlPrtlsBox);
      $('#plExtractorBox').css("display", "none").addClass("mobile");

      if(window.useAndroidPanes())
        android.addPane("plugin-plExtractor", "Portal List Extractor", "ic_action_star");
      window.addHook('paneChanged', window.plugin.plExtractor.onPaneChanged);
    }
    // DELETE autodraw box
    $('#toolbox').append(window.plugin.plExtractor.htmlCallSetBox/*+window.plugin.plExtractor.htmlCalldrawBox*/);

    if(window.plugin.plExtractor.isSmart) {
//      $('#plExtractorBox.mobile #topBar').prepend(window.plugin.plExtractor.htmlCallSetBox+window.plugin.plExtractor.htmlCalldrawBox); // wonk in progress
      $('#plExtractorBox.mobile #topBar').append(plugin.plExtractor.htmlMoveBtn);
    }

    // DELETE
    //window.plugin.plExtractor.loadList('maps');
    window.plugin.plExtractor.loadList('portals');
    window.plugin.plExtractor.jquerySortableScript();

    if(window.plugin.plExtractor.statusBox['show'] === 0) { window.plugin.plExtractor.switchStatusPrtlsBox(0); }
    if(window.plugin.plExtractor.statusBox['page'] === 1) { $('#plExtractorBox h5.prtl_portals').trigger('click'); }

    window.addHook('portalSelected', window.plugin.plExtractor.onPortalSelected);
    window.addHook('search', window.plugin.plExtractor.onSearch);

    // Sync
    window.addHook('pluginPlExtractorEdit', window.plugin.plExtractor.syncPrtls);
    window.addHook('iitcLoaded', window.plugin.plExtractor.registerFieldForSyncing);

    // Highlighter - portal to extract
    window.addHook('pluginPlExtractorEdit', window.plugin.plExtractor.highlightRefresh);
    window.addHook('pluginPlExtractorSyncEnd', window.plugin.plExtractor.highlightRefresh);
    // TODO verifier que mon premier parametre est bon ici
    window.addPortalHighlighter('Portal List Extractor', window.plugin.plExtractor.highlight);

    // Layer - Bookmarked portals
    window.plugin.plExtractor.starLayerGroup = new L.LayerGroup();
    // TODO verifier que mon premier parametre est bon ici
    window.addLayerGroup('Portals to Extract', window.plugin.plExtractor.starLayerGroup, false);
    window.plugin.plExtractor.addAllStars();
    window.addHook('pluginPlExtractorEdit', window.plugin.plExtractor.editStar);
    window.addHook('pluginPlExtractorSyncEnd', window.plugin.plExtractor.resetAllStars);

    if(window.plugin.portalslist) {
      window.plugin.plExtractor.setupPortalsList();
    } else {
      setTimeout(function() {
        if(window.plugin.portalslist)
          window.plugin.plExtractor.setupPortalsList();
      }, 500);
    }
  };

// PLUGIN END //////////////////////////////////////////////////////////


setup.info = plugin_info; //add the script info data to the function as a property
if(!window.bootPlugins) window.bootPlugins = [];
window.bootPlugins.push(setup);
// if IITC has already booted, immediately run the 'setup' function
if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);


