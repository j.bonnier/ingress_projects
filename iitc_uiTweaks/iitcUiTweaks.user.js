// ==UserScript==
// @id             iitc-plugin-uiTweaks
// @name           IITC plugin: UI Tweaks
// @category       Tweaks
// @version        0.0.1.20170102.1106
// @namespace      https://github.com/jonatkins/ingress-intel-total-conversion
// @updateURL      https://gitlab.com/j.bonnier/ingress_projects/raw/master/iitc_uiTweaks/iitcUiTweaks.user.js
// @downloadURL    https://gitlab.com/j.bonnier/ingress_projects/raw/master/iitc_uiTweaks/iitcUiTweaks.user.js
// @description    [iitc-2016-10-03-004740] Add some minor tweaks to iitc ui.
// @include        https://*.ingress.com/intel*
// @include        http://*.ingress.com/intel*
// @match          https://*.ingress.com/intel*
// @match          http://*.ingress.com/intel*
// @include        https://*.ingress.com/mission/*
// @include        http://*.ingress.com/mission/*
// @match          https://*.ingress.com/mission/*
// @match          http://*.ingress.com/mission/*
// @grant          none
// ==/UserScript==

function wrapper(plugin_info) {
    // ensure plugin framework is there, even if iitc is not yet loaded
    if(typeof window.plugin !== 'function') window.plugin = function() {};
    if(typeof map === 'undefined') map = {};
    if(typeof L === 'undefined') L = {};
    if(typeof google === 'undefined') google = {};
    if(typeof android === 'undefined') android = false;
    if(typeof GM_info === 'undefined') GM_info = {};
    
    //PLUGIN AUTHORS: writing a plugin outside of the IITC build environment? if so, delete these lines!!
    //(leaving them in place might break the 'About IITC' page or break update checks)
    plugin_info.buildName = 'iitc';
    plugin_info.dateTimeVersion = '20170102.1106';
    plugin_info.pluginId = 'uiTweaks';
    //END PLUGIN AUTHORS NOTE

    // PLUGIN START ////////////////////////////////////////////////////////
    /***********************************************************************

        HOOKS:
        - pluginUiTweaksEdit: fired when options are saved to localStorage;
        - pluginUiTweaksSyncEnd: fired when the sync is finished;

    ***********************************************************************/
    ////////////////////////////////////////////////////////////////////////

    // use own namespace for plugin
    window.plugin.uiTweaks = function() {};

    window.plugin.uiTweaks.UPDATE_QUEUE = {key: 'plugin-uiTweaks-queue', field: 'updateQueue'};
    window.plugin.uiTweaks.UPDATING_QUEUE = {key: 'plugin-uiTweaks-updating-queue', field: 'updatingQueue'};

    window.plugin.uiTweaks.options = {}; // Will be overwrited later in the code.
    window.plugin.uiTweaks.INIT = false;

   /* window.plugin.uiTweaks.statusBox = {};*/
    window.plugin.uiTweaks.updateQueue = {};
    window.plugin.uiTweaks.updatingQueue = {};
    
    window.plugin.uiTweaks.starLayers = {};
    window.plugin.uiTweaks.starLayerGroup = null;

    window.plugin.uiTweaks.isSmart = undefined;
    window.plugin.uiTweaks.isAndroid = function() {
        if (typeof android !== 'undefined' && android) {
            return true;
        }
        return false;
    };

    // DEBUG
    window.plugin.uiTweaks.DEBUG = false;
    window.plugin.uiTweaks.LINE_ADD = 27;
    window.plugin.uiTweaks.getCurrentLine = function() {
        try { throw Error(''); }
        catch(err) {
            return parseInt(err.stack.split('\n')[2].split(':')[1]) + window.plugin.uiTweaks.LINE_ADD;
        }
    };

    /**********************
     * STORAGE
     **********************/
    window.plugin.uiTweaks.storage = function() {};
    window.plugin.uiTweaks.storage.KEY = 'plugin-uiTweaks';
    window.plugin.uiTweaks.KEY = {key: window.plugin.uiTweaks.storage.KEY, field: 'options'};
    /**
     * Updates the localStorage
     * @returns {undefined}
     */ 
    window.plugin.uiTweaks.storage.save = function() {
        try {
            localStorage[plugin.uiTweaks.storage.KEY] = JSON.stringify(window.plugin.uiTweaks.options);
            if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.storage.save\n\twindow.plugin.uiTweaks.options: %o\n\tJSON.stringify(window.plugin.uiTweaks.options): %s', window.plugin.uiTweaks.options, JSON.stringify(window.plugin.uiTweaks.options));
            window.runHooks('pluginUiTweaksEdit');
        }
        catch (e) {
            if (window.plugin.uiTweaks.DEBUG) console.debug("uiTweaks.storage.save\n\tError: %o", e);
        }
    };
    /**
     * Loads the localStorage.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.storage.load = function() {
        try {
            if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.storage.load\n\twindow.plugin.uiTweaks.options: %o\n\tlocalStorage[plugin.uiTweaks.storage.KEY]: %o\n\tJSON.parse(localStorage[plugin.uiTweaks.storage.KEY]): %o',
                                                    window.plugin.uiTweaks.options,
                                                    localStorage[window.plugin.uiTweaks.storage.KEY],
                                                    JSON.parse(localStorage[window.plugin.uiTweaks.storage.KEY]));
            window.plugin.uiTweaks.loadOptions(JSON.parse(localStorage[window.plugin.uiTweaks.storage.KEY]));
        }
        catch(e) {
            if (window.plugin.uiTweaks.DEBUG) console.debug("uiTweaks.storage.load\n\tError: %o\n\n\tWill try to create storage.", e);
            try {
                window.plugin.uiTweaks.storage.create();
            }
            catch(er) {}
        }
    };
    /**
     * Creates the localStorage.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.storage.create = function() {
        if(!localStorage[window.plugin.uiTweaks.storage.KEY]) {
            window.plugin.uiTweaks.storage.save();
        }
    };
    
    /**********************
     * CHAT MINIZER
     **********************/
    window.plugin.uiTweaks.chatMnmzr = function() {};
    window.plugin.uiTweaks.chatMnmzr.enabled = true;
    window.plugin.uiTweaks.chatMnmzr.expanded = false;
    /**
     * Initializes the chat minizer.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.chatMnmzr.setup = function() {
        var id = $('#chatcontrols > a').length;
        var html = '<a accesskey="'+id+'" title="['+id+']" class="hide" onclick="window.plugin.uiTweaks.chatMnmzr.toggle();return false;"><span class="toggle hide"></span></a>';
        $('#chatcontrols').append(html);
    };
    /**
     * Removes the chat minimizer.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.chatMnmzr.setdown = function() {
        if ($('#chatcontrols').hasClass('hidden')) window.plugin.uiTweaks.chatMnmzr.toggle();
        $('#chatcontrols a.hide').remove();
    };
    /**
     * Toggles the chat minimizer.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.chatMnmzr.toggle = function() {
        if ($('#chat, #chatcontrols').hasClass('expand') || window.plugin.uiTweaks.chatMnmzr.expanded) {
            window.plugin.uiTweaks.chatMnmzr.expanded = !(window.plugin.uiTweaks.chatMnmzr.expanded);
            if (typeof window.chat.toggle === 'function') {
               window.chat.toggle();
            }
        }
        $('#chat, #chatinput, #chatcontrols, #chatcontrols a').toggleClass('hidden');
    };
        
    /******************************************
     * ADVANCED SCORE
     * Adds region (cell) score to sidebar.
     ******************************************/
    window.plugin.uiTweaks.advScore = function() {};
    window.plugin.uiTweaks.advScore.enabled = true;
    window.plugin.uiTweaks.advScore.currentScope = 'global';
    window.plugin.uiTweaks.advScore.refreshDelay = 100;  //milisecondes
    window.plugin.uiTweaks.advScore.lastRefresh = 0;
    window.plugin.uiTweaks.advScore.regionPolygon = null;
    window.plugin.uiTweaks.advScore.showCell = true;
    /**
     * Initializes advanced score.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.advScore.setup = function() {
        var html = '<div id="advScore"><div class="title global">Global Score</div><div class="global-region"><a accesskey="r" onclick="window.plugin.uiTweaks.advScore.regionToggle();return false;">region</a></div></div>';
        $(html).insertBefore('div#gamestat');
    };
    /**
     * Removes advanced score.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.advScore.setdown = function () {
        if (window.plugin.uiTweaks.advScore.currentScope === 'region') window.plugin.uiTweaks.advScore.regionToggle();
        $('#advScore').remove();
    };
    /**
     * Determines whether or not it should send a refresh request.
     * This is to avoid huge amount of requests when using mouse scrolling.
     * TODO : find a better solution?
     * @returns {Boolean}
     */
    window.plugin.uiTweaks.advScore.refresh = function() {
        var d = new Date();
        var lr = window.plugin.uiTweaks.advScore.lastRefresh;
        var rd = window.plugin.uiTweaks.advScore.refreshDelay;
        if (d - lr  >= rd) {
            window.plugin.uiTweaks.advScore.lastRefresh = d;
            if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.advScore.refresh\n\tRefreshing: %d >= %d', (d-lr), rd);
            return true;
        }
        else {
            if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.advScore.refresh\n\tNo refresh: %d < %d', (d-lr), rd);
            return false;
        }
    };
    /**
     * Toggles UI and env from region to global and vice versa.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.advScore.regionToggle = function() {
        var cur = window.plugin.uiTweaks.advScore.currentScope;
        if (cur === 'region') {
            if (window.plugin.uiTweaks.advScore.regionPolygon !== null) {
                map.removeLayer(window.plugin.uiTweaks.advScore.regionPolygon);
                window.plugin.uiTweaks.advScore.regionPolygon = null;
            }
        }
        var title = ( cur === 'global') ? 'Loading Region Score...' : 'Global Score';
        var button = ( cur === 'global') ? 'global' : 'region';
        var buttonHtml = '<a accesskey="" title="" class="" onclick="window.plugin.uiTweaks.advScore.regionToggle();return false;">'+ button +'</a>';
        $('#advScore .title').html(title);
        $('#advScore .global-region').html(buttonHtml);
        $('#advScore .title').toggleClass('global region');
        window.plugin.uiTweaks.advScore.currentScope = (cur === 'global') ? 'region' : 'global';
        window.plugin.uiTweaks.advScore.update();
    };
    /**
     * Performs the update on UI and env on update success.
     * @param {Object} data
     * @returns {unresolved}
     */
    window.plugin.uiTweaks.advScore.updateSuccess = function(data) {
        if (typeof data.result === 'undefined') {
            return window.plugin.uiTweaks.advScore.updateFailure();
        }
        var cur = window.plugin.uiTweaks.advScore.currentScope;
        var regionName = data.result.regionName;
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.advScore.updateSuccess\n\tdata: %o', data);
        if (cur === 'region') {
            $('#advScore .title').html(regionName);
            var e = parseInt(data.result.gameScore[0]); //enlightened score in result[0]
            var r = parseInt(data.result.gameScore[1]); //resistance score in result[1]
            var s = r+e;
            var rp = r/s*100, ep = e/s*100;
            r = digits(r), e = digits(e);
            var rs = '<span class="res" style="width:'+rp+'%;">'+Math.round(rp)+'%&nbsp;</span>';
            var es = '<span class="enl" style="width:'+ep+'%;">&nbsp;'+Math.round(ep)+'%</span>';
            $('#gamestat').html(rs+es).on('click', function() { window.plugin.uiTweaks.advScore.update(); });
            // help cursor via “#gamestat span”
            $('#gamestat').attr('title', 'Resistance:\t'+r+' MindUnits\nEnlightened:\t'+e+' MindUnits');
            window.plugin.uiTweaks.advScore.drawCell(data);
        }
        window.map.on('moveend', window.plugin.uiTweaks.advScore.update);
    };
    /**
     * Draws the active cell on map.
     * @param {Object} data
     * @returns {undefined}
     */
    window.plugin.uiTweaks.advScore.drawCell = function(data) {
        var polygon = window.plugin.uiTweaks.advScore.regionPolygon;
        if (window.plugin.uiTweaks.advScore.showCell !== true) {
            if (polygon !== null) {
                map.removeLayer(polygon);
                window.plugin.uiTweaks.advScore.regionPolygon = null;
            }
            return;
        }
        var latlngs = data.result.regionVertices;
        for (var i = 0; i < latlngs.length; i++)
        {
            latlngs[i][0] = latlngs[i][0] / 1.E6;
            latlngs[i][1] = latlngs[i][1] / 1.E6;
        }
        var polyopts = {
            stroke: true,
            color: '#dd9700',
            weight: 1.5,
            opacity: 0.9,
            fill: false,
            fillColor: null, //same as color by default
            fillOpacity: 0,
            clickable: false
        };
        if (polygon !== null) map.removeLayer(polygon);
        polygon = L.polygon(latlngs, polyopts).addTo(map);
        window.plugin.uiTweaks.advScore.regionPolygon = polygon;
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.advScore.drawCell\n\tpolygon: %o\n\tmap: %o', polygon, map);
    };
    /**
     * Sends an update request.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.advScore.update = function() {
        if (!window.plugin.uiTweaks.advScore.refresh()) return;
        var cur = ( $('#advScore .title').hasClass('global') ) ? 'global' : 'region';
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.advScore.update\n\t Updating: %s', cur);
        if (cur === 'global') {
            window.updateGameScore();
            return;
        }
        var latLng = map.getCenter();
        var latE6 = Math.round(latLng.lat*1E6);
        var lngE6 = Math.round(latLng.lng*1E6);
        window.postAjax('getRegionScoreDetails', {latE6:latE6,lngE6:lngE6}, function(res){window.plugin.uiTweaks.advScore.updateSuccess(res);}, function(){window.plugin.uiTweaks.advScore.updateFailure();});
        
    };
    /**
     * Logs Region Score Update Failures.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.advScore.updateFailure = function () {
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.advScore.update\n\tUpdate failed.');
    };
    
    /*********************
     * STREET VIEW
     *********************/
    window.plugin.uiTweaks.streetView = function() {};
    window.plugin.uiTweaks.streetView.enabled = true;
    window.plugin.uiTweaks.streetView.active = false;
    window.plugin.uiTweaks.streetView.iconPosition = {top: 0, left: 0};
    /**
     * Initializes street view.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.streetView.setup = function() {
        var html = '<a class="leaflet-control-streetview" onclick="window.plugin.uiTweaks.streetView.toggle();return false;"><span class="icon"></span></a>';
        $(html).insertBefore('.leaflet-control-zoom-in');
        $('.leaflet-control-streetview .icon').draggable({
            start: function() {
                window.plugin.uiTweaks.streetView.drag();
            },
            drag: function() {
                window.plugin.uiTweaks.streetView.dragging();
            },
            stop: function(e) {
                window.plugin.uiTweaks.streetView.drop(e);
            }
        });
        $('.leaflet-control-streetview .icon').mousedown(function() { map.dragging.disable(); });
        $('.leaflet-control-streetview .icon').mouseup(function () { map.dragging.enable(); });
        window.plugin.uiTweaks.streetView.iconPosition = $('.leaflet-control-streetview .icon').position();
    };
    /**
     * Removes street view support.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.streetView.setdown = function () {
        $('.leaflet-control-streetview').remove();
    };
    /**
     * Toggles streetview on/off.
     * @param {lat,Lng} latLng
     * @returns {undefined}
     */
    window.plugin.uiTweaks.streetView.toggle = function (latLng) {
        if (window.plugin.uiTweaks.streetView.active) window.plugin.uiTweaks.streetView.turnOff();
        else window.plugin.uiTweaks.streetView.turnOn(latLng);
    };
    /**
     * Turns streetview on.
     * @param {type} latLng
     * @returns {undefined}
     */
    window.plugin.uiTweaks.streetView.turnOn = function (latLng) {
        if (typeof latLng === 'undefined') {
            var center = map.getCenter();
            latLng = center;
        }
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.streetView.turnOn\n\tmap: %o\n\tlatLng: %s\n\tL: %o', map, latLng, L);
        var html = '';
        html += '<div id="googleMap"></div><div id="streetView"></div>';
        $('body').append(html);
        $('#streetView').css({zIndex: 9998, position: 'absolute'});
        var gmap = new google.maps.Map(document.getElementById('googleMap'), {
            center: latLng,
            zoom: 14
        });
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.streetView.turnOn\ngmap: %o', gmap);
        var panorama = new google.maps.StreetViewPanorama(
            document.getElementById('streetView'), {
                position: latLng,
                pov: {
                    heading: 34,
                    pitch: 10
                }
        });
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.streetView.turnOn\n\tpanorama: %o', panorama);
        gmap.setStreetView(panorama);
        window.plugin.uiTweaks.streetView.toggleToolbar(true);
        $('body').bind('keyup', window.plugin.uiTweaks.streetView.escHandler);
        window.plugin.uiTweaks.streetView.escPopup();
        window.plugin.uiTweaks.streetView.active = true;
    };
    /**
     * Turns streetview off.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.streetView.turnOff = function () {
        $('#streetView, #googleMap').remove();
        window.plugin.uiTweaks.streetView.toggleToolbar(false);
        $('body').unbind('keyup', window.plugin.uiTweaks.streetView.escHandler);
        window.plugin.uiTweaks.streetView.active = false;       
    };
    /**
     * Displays a popup to inform about how to exit street view mode.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.streetView.escPopup = function () {
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.streetView.escPopup\n\tDisplaying popup.');
        $('body').append('<div id="uiTweaks-streetView-esc-popup">Press ESC to exit street view mode.</div>');

        var x = (($(window).width() - $('#uiTweaks-streetView-esc-popup').outerWidth())/2);
        var y = (($(window).height() - $('#uiTweaks-streetView-esc-popup').outerHeight())/2);
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.streetView.escPopup\n\tx: %o\n\ty: %o', x, y);
        $('#uiTweaks-streetView-esc-popup').css({
            zIndex: 9999,
            position: 'absolute',
            left: x,
            top: y
        });
        $('#uiTweaks-streetView-esc-popup').fadeOut(1000, function () { $(this).remove(); });
    };
    /**
     * Handles the esc key up event.
     * @param {event object} e
     * @returns {undefined}
     */
    window.plugin.uiTweaks.streetView.escHandler = function (e) {
        if (window.plugin.uiTweaks.DEBUG) console.debug('window.plugin.uiTweaks.streetView.escHandler\n\te: %o', e);
        if (e.keyCode === 27) window.plugin.uiTweaks.streetView.turnOff();
    };
    /**
     * Toggles toolbar icons on and off.
     * @param {Boolean} active
     * @returns {undefined}
     */
    window.plugin.uiTweaks.streetView.toggleToolbar = function (active) {
        if (typeof active !== 'Boolean') active = !window.plugin.uiTweaks.streetView.active;
        if (active === true) $('.leaflet-control-container').hide();
        else $('.leaflet-control-container').show();
    };
    /**
     * Handles icon dragging event.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.streetView.dragging = function () {};
    /**
     * Handles icon drop event.
     * @param {type} e
     * @returns {undefined}
     */
    window.plugin.uiTweaks.streetView.drop = function (e) {
        var p = new L.point(e.clientX, e.clientY);
        var latLng = map.layerPointToLatLng(p);
        var position = window.plugin.uiTweaks.streetView.iconPosition;
        window.plugin.uiTweaks.streetView.toggle(latLng);
        $('.leaflet-control-streetview .icon').css({left: position.left, top: position.top});
    };
    /**
     * Handles icon drag start event.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.streetView.drag = function() {};
    
    /*********************
     * SYNC
     *********************/
    window.plugin.uiTweaks.SYNC_DELAY = 5000;
    window.plugin.uiTweaks.sync = function () {};
    window.plugin.uiTweaks.sync.enabled = true;
    /**
     * Enables sync for the plugin.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.sync.setup = function () {
        if (typeof window.plugin.sync === 'undefined') {
            console.log('Missing iitc plugin: sync.');
            if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.sync.setup\n\tAttempting to turn sync on for the plugin, however, sync is not installed or active.');
            window.plugin.uiTweaks.sync.enabled = false;
            // Do not alter sync.enabled in option so settings still ok when
            // sync will be installed or activated.
            return;
        }
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.sync.setup\n\tTurning sync on for this plugin.');
        window.plugin.uiTweaks.storage.save();
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.sync.setup\n\tWill sync options now.');
        window.plugin.uiTweaks.sync.syncOptions();  
    };
    /**
     * Disables sync for the plugin.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.sync.setdown = function () {
        try {
            clearTimeout(plugin.uiTweaks.sync.delaySync.timer);
            if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.sync.setdown\n\tTimeout cleared!');
        }
        catch (e) {
            if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.sync.setdown\n\tUnabled to clearTimeout\n\t\t%o', e);
        }
    };
    /**
     * Delays the syncing to group a few updates in a single request
     * @returns {undefined}
     */
    window.plugin.uiTweaks.sync.delaySync = function() {
        if (window.plugin.uiTweaks.sync.enabled !== true) return;
        clearTimeout(plugin.uiTweaks.sync.delaySync.timer);
        window.plugin.uiTweaks.sync.delaySync.timer = setTimeout(function() {
            window.plugin.uiTweaks.sync.delaySync.timer = null;
            window.plugin.uiTweaks.sync.syncNow();
        }, window.plugin.uiTweaks.SYNC_DELAY);
    };
    /**
     * Stores the updateQueue in updatingQueue and upload
     * @returns {undefined}
     */
    window.plugin.uiTweaks.sync.syncNow = function() {
        if (window.plugin.uiTweaks.INIT !== true) window.plugin.uiTweaks.sync.delaySync(); return;
        if (window.plugin.uiTweaks.sync.enabled !== true) return;
        $.extend(window.plugin.uiTweaks.updatingQueue, window.plugin.uiTweaks.updateQueue);
        window.plugin.uiTweaks.updateQueue = {};
        window.plugin.uiTweaks.sync.storeLocal(window.plugin.uiTweaks.UPDATING_QUEUE);
        window.plugin.uiTweaks.sync.storeLocal(window.plugin.uiTweaks.UPDATE_QUEUE);

        window.plugin.sync.updateMap('uiTweaks', window.plugin.uiTweaks.KEY.field, Object.keys(window.plugin.uiTweaks.updatingQueue));
    };
    /**
     * Call after IITC and all plugin loaded
     * @returns {undefined}
     */
    window.plugin.uiTweaks.sync.registerField = function() {
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.sync.registerField\n\twindow.plugin.sync: %o', window.plugin.sync);
        if (typeof window.plugin.sync === 'undefined') return;
        window.plugin.sync.registerMapForSync('uiTweaks', window.plugin.uiTweaks.KEY.field, window.plugin.uiTweaks.sync.callback, window.plugin.uiTweaks.sync.initialized);
    };
    /**
     * Call after local or remote change uploaded.
     * 
     * @param {string} pluginName
     * @param {string} fieldName
     * @param {object} e
     * @param {type} fullUpdated
     * @returns {undefined}
     */
    window.plugin.uiTweaks.sync.callback = function(pluginName, fieldName, e, fullUpdated) {
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.sync.callback\n\tpluginName: %s\n\tfieldName: %s\n\te: %o\n\tfullUpdated: %o', pluginName, fieldName, e, fullUpdated);
        if (fieldName === window.plugin.uiTweaks.KEY.field) {
            window.plugin.uiTweaks.sync.storeLocal(window.plugin.uiTweaks.KEY);
            // All data is replaced if other client update the data during this client offline,
            if (fullUpdated) {
                window.plugin.uiTweaks.sync.syncOptions();
                return;
            }
            if (!e) return;
            if (e.isLocal) {
                // Update pushed successfully, remove it from updatingQueue
                delete window.plugin.uiTweaks.updatingQueue[e.property];
            } else {
                // Remote update
                delete window.plugin.uiTweaks.updateQueue[e.property];
                window.plugin.uiTweaks.sync.storeLocal(window.plugin.uiTweaks.UPDATE_QUEUE);
                //window.plugin.uiTweaks.refreshPrtls();
                window.runHooks('pluginUiTweaksSyncEnd', {"target": "all", "action": "sync"});
                if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.sync.callback\n\tSynced all');
            }
        }
    };
    /**
     * Syncing of the field is initialized, upload all queued update.
     * 
     * @param {string} pluginName
     * @param {string} fieldName
     * @returns {undefined}
     */
    window.plugin.uiTweaks.sync.initialized = function(pluginName, fieldName) {
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.sync.initialized\n\tpluginName: %s\n\tfieldName: %s\n\twindow.plugin.uiTweaks.updateQueue: %o', pluginName, fieldName, window.plugin.uiTweaks.updateQueue);
        if (fieldName === window.plugin.uiTweaks.KEY.field) {
            window.plugin.uiTweaks.sync.enabled = true;
            if (Object.keys(window.plugin.uiTweaks.updateQueue).length > 0) {
                window.plugin.uiTweaks.sync.delaySync();
            }
        }
    };
    // TODO : checker ça
    window.plugin.uiTweaks.sync.storeLocal = function(mapping) {
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.sync.storeLocal\n\tmapping: %o', mapping);
        if (typeof(window.plugin.uiTweaks[mapping.field]) !== 'undefined' && window.plugin.uiTweaks[mapping.field] !== null) {
            localStorage[mapping.key] = JSON.stringify(window.plugin.uiTweaks[mapping.field]);
        } else {
            localStorage.removeItem(mapping.key);
        }
    };
    // TODO : checker ça
    window.plugin.uiTweaks.sync.loadLocal = function(mapping) {
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.sync.loadLocal\n\tmapping: %o', mapping);
        var objectJSON = localStorage[mapping.key];
        if (!objectJSON) return;
        window.plugin.uiTweaks[mapping.field] = mapping.convertFunc
                                ? mapping.convertFunc(JSON.parse(objectJSON))
                                : JSON.parse(objectJSON);
    };
    // TODO : checker ça
    window.plugin.uiTweaks.sync.syncOptions = function() {
        window.plugin.uiTweaks.sync.loadLocal(window.plugin.uiTweaks.KEY);

        window.plugin.uiTweaks.updateQueue = window.plugin.uiTweaks.options;
        window.plugin.uiTweaks.sync.storeLocal(window.plugin.uiTweaks.UPDATE_QUEUE);

        window.plugin.uiTweaks.sync.delaySync();
    };
    
    /*********************
     * OPTIONS
     *********************/
    window.plugin.uiTweaks.options = {
        chatMnmzr: {
            __name: 'Chat Minimizer',
            enabled: window.plugin.uiTweaks.chatMnmzr.enabled
        },
        advScore: {
            __name: 'Advanced Score',
            enabled: window.plugin.uiTweaks.advScore.enabled,
            showCell: window.plugin.uiTweaks.advScore.showCell
        },
        streetView: {
            __name: 'Street View',
            enabled: window.plugin.uiTweaks.streetView.enabled
        },
        sync: {
            __name: 'Sync',
            enabled: window.plugin.uiTweaks.sync.enabled
        }
    };
    /**
     * Returns the link to open the options window.
     * @returns {String}
     */
    window.plugin.uiTweaks.htmlCallSetBox = function () {
        return '<a onclick="window.plugin.uiTweaks.optionsWindow();return false;" title="uiTweaks Options">uiTweaks Opt</a>';
    };
    /**
     * Creates options window dialog.
     * @returns {Window.plugin.uiTweaks.optionsWindow.html|String}
     */
    window.plugin.uiTweaks.optionsWindow = function() {
        var opts = window.plugin.uiTweaks.options;
        var html = '';
        html += '<div id="uiTweaksOptions"><ul>';
        for (var key in opts) {
            if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.optionsWindow\n\tkey: %o\n\topts: %o', key, opts);
            html += '<li>'+ opts[key].__name +'<ul>';
            for (var k in opts[key]) {
                if (k !== '__name') {
                    html += '<li><input type="checkbox" id="uiTweaks-'+ key +'-'+ k +'" name="'+ key +'" value="'+ k +'"'+ (opts[key][k] === true ? 'checked="checked"' : null );
                        html += ' onclick="window.plugin.uiTweaks.setOption(\'uiTweaks.'+ key +'.'+ k +'\', $(\'#uiTweaks-'+ key +'-'+ k +'\').prop(\'checked\'), true);"></input>';
                    html += '<label for="uiTweaks.'+ key +'.'+ k +'">'+ k +'</label></li>';
                }
            }
            html += '</ul></li>';
        }
        html += '</ul></div>';
        dialog({
            html: html,
            dialogClass: 'uiTweaks-dialog-options',
            title: 'IITC UI Tweaks Options'
        });
    };
    /**
     * Sets an option and optionally update localStorage.
     * 
     * @param {string} option
     * @param {undeifined} value
     * @param {Boolean} update (optional)
     * @returns {Boolean|undefined}
     */
    window.plugin.uiTweaks.setOption = function (option, value, update) {
        var opt = option.split('.');
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.setOption\n\toptions: %o\n\tvalue: %o\n\topt: %o',option, value, opt);
        if (opt.length !== 3 || opt[0] !== 'uiTweaks') return false;
        
        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.setOption\n\toption: %s\n\tvalue: %o\n\tupdate: %o\n\twindow.plugin.uiTweaks.INIT: %o', option, value, update, window.plugin.uiTweaks.INIT);
        
        if (typeof window.plugin.uiTweaks[opt[1]][opt[2]] !== 'undefined' && typeof window.plugin.uiTweaks.options[opt[1]][opt[2]] !== 'undefined') {
            window.plugin.uiTweaks[opt[1]][opt[2]] = value;
            window.plugin.uiTweaks.options[opt[1]][opt[2]] = value;         
            if (opt[2] === 'enabled') {
                if (value === true && window.plugin.uiTweaks.INIT === true) window.plugin.uiTweaks[opt[1]].setup();
                else window.plugin.uiTweaks[opt[1]].setdown();
            } 
            if (update === true) window.plugin.uiTweaks.storage.save();
        }
        else if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.setOption\n\topt: %o', opt);
        return;
    };
    /**
     * Loads options.
     * 
     * @param {object} input (options)
     * @param {Boolean} update (optional)
     * @returns {undefined}
     */
    window.plugin.uiTweaks.loadOptions = function (input, update) {
        if (typeof update === 'undefined' || update !== true) update = false;
        var opts = window.plugin.uiTweaks.options;

        if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.loadOptions\n\topts: %o\n\tinput: %on\tupdate: %o', opts, input, update);

        for (var opt in opts) {
            for (var o in opts[opt]) {
                if (o !== '__name') {
                    var p = 'uiTweaks.'+ opt +'.'+ o;
                    if (window.plugin.uiTweaks.DEBUG) console.debug('uiTweaks.loadOptions\n\topt: %s\n\to: %s\n\toptions[opt][o]: %s\n\tp: %s', opt, o, input[opt][o], p);
                    window.plugin.uiTweaks.setOption(p, input[opt][o]);
                }
            }
        }
        if (update === true) window.plugin.uiTweaks.storage.save();
    };
    
    /**
     * Setups the CSS for the plugin.
     * @returns {undefined}
     */
    window.plugin.uiTweaks.setupCSS = function() {
        $('<style>').prop('type', 'text/css').html('#chatcontrols.hidden{bottom:0;}#chat.hidden,#chatinput.hidden,#chatcontrols a.hidden{display:none;}#chatcontrols a.hide.hidden{display:block;bottom:0;}#chatcontrols a span.hide{width:0;height:0;border-style:solid;border-width:10px 0 0 10px;border-color:transparent transparent transparent #FFCE00;}#chatcontrols a.hidden span.hide{border-width:0 10px 10px 0;border-color:transparent #FFCE00 transparent transparent;}#chatcontrols a.hide{width:40px;/*letter-spacing:-1px; text-decoration:none !important;*/}#advScore{display:flex;justify-content:space-between;}#advScore div{display:inline-block;}#advScore .title{margin:auto auto auto 5px;font-family:"Roboto", "Helvetica Neue", Helvetica, sans-serif;font-weight:bold;color:#ffce00;font-size:14px;}#advScore .global-region{position:relative;padding:2px 5px;line-height:15px;margin:5px 5px 0 auto;background-color:rgba(8, 48, 78, 0.9);color:#FFCE00;border:1px solid #20A8B1;border-bottom:none;border-radius:5px 5px 0 0;text-decoration:none;}#googleMap{display:none;}#googleMap,#streetView{position:absolute;top:0;right:0;left:0;bottom:0;}.leaflet-control-streetview,.leaflet-control-streetview .icon{background-image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAQCAYAAAArij59AAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAYRJREFUeNpUUD1rG1EQnN19z+jDZ0dXHCpUpBOkMxijf5AyUpMiKlTIJG1QEQxu0wQTVYEUqQQC1cGodhc3LkwgjZpUEgpXnMJZDyTu3qbRHWSrYXd2mBlSVQDAcDi8iOP4AwBqNBqfJ5PJDwAgVcVoNHqxWCx+4TCqina7fTYejx8ZAFar1VWWZTDGwBiDPM+xXC6vAYABwHt/TESFAA64XhKazeZN8ZnnOUQEURR9Kj0AQL/fv0zT9AoA1+v1m9ls9rUkDAaDj3Ecv2LmZ6oKVU3CMLyfTqfvSFXR6/X+7Pf76HCEiEBEslardSTr9fr5ZrO5ZmYYY8DMYGZkWcZBEHxn59wba62rVqsP3nsAgLX2pzHmyTn32my32/eVSuWbqp6o6vkh5u9arXbnnLuUTqdjgyD4kiTJiIhOiWhHRKdRFL0FsCljdrvd3Fp7LyJPaZq+nM/nBACmaM97vxeRRFX/WmvLVrkA1tqK9x7M7Auz/ynsdrvbMAxvVXULICr2/wYA4FKu1Yvj7DgAAAAASUVORK5CYII=");}.leaflet-control-streetview .icon:hover{background-image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAQCAYAAAArij59AAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAclJREFUeNo8wU9IFGEYwOHf+8246ezM7qqr9g8xwSRJ0JCki0GXjh2CIM9J0SESIoLo1iUq8BJBUkH3ILBbh+rSBgWKqFGoS2JKJtruzK7r7sz3dut5xEarYFy0+OS0br65DQj5s4+d/nufcHzEht+gvDhgF28tgoIYsDFmcGpYciNzRsvz2PVXd4hL0JQFN4Ckgq49v2t3ChhJ9iCp+eDwnxhQmxYxGPwB5Mj4Q2nOYihjiJAmDw5dfIDXiyHdg7QNF+LmMxONmrcS73vFODVyXYL+j/h9iP79Ar+e3a8svLuws3owp1ZpO/Zn1z85WqB78qpLuET8e27Cy9c6w40Q24B01/7ReGvhhBN8vuZqfbPHVrc7pSWgtd+AKmoy2Ch0nSQaMhoujzvp9qoe6P2a8qqk/DrW6Zo3LUGk4fIlV8tLN8U7PC2QsZXGCEYRT4qS7Xuv0Y8rrrSOTtHS/VLXXrylKVNFE0tSHaLj8g0pza5LUnwKatGf0wmpXAEnHVGaPS/tY0LbGC7VNYi+Q1Kr4/i7aFIilUfy58ANcAmOI7lT6MqjZpI9xM1YTWro9gdAcXXjNSoGalsz0jE4gzYqiOkkOwTi8G8AqX7E35aJeLMAAAAASUVORK5CYII=");}.leaflet-control-streetview .icon{width:100%;height:100%;display:block;background-repeat:no-repeat;background-position:center;}#uiTweaksOptions ul{list-style:none;padding:0;margin:0;}#uiTweaks-streetView-esc-popup{display:block;width:400px;text-align:center;margin:auto;padding:20px;font-size:24px;color:#dddddd;background-color:#333333;}').appendTo('head');
    };

/***************************************************************************************************************************************************************/

  var setup = function() {
    window.plugin.uiTweaks.isSmart = window.isSmartphone();

    // Fired when options are saved to localStorage
    if($.inArray('pluginUiTweaksEdit', window.VALID_HOOKS) < 0) { window.VALID_HOOKS.push('pluginUiTweaksEdit'); }
    // Fired when the sync is finished;
    if($.inArray('pluginUiTweaksSyncEnd', window.VALID_HOOKS) < 0) { window.VALID_HOOKS.push('pluginUiTweaksSyncEnd'); }

    // If the storage not exists or is a old version
    /*window.plugin.uiTweaks.createStorage();
    window.plugin.uiTweaks.upgradeToNewStorage();*/

    // Load data from localStorage
    window.plugin.uiTweaks.storage.load();
    window.plugin.uiTweaks.setupCSS();
    if(!window.plugin.uiTweaks.isSmart) {
        if (window.plugin.uiTweaks.chatMnmzr.enabled) window.plugin.uiTweaks.chatMnmzr.setup();
        if (window.plugin.uiTweaks.advScore.enabled) window.plugin.uiTweaks.advScore.setup();
        if (window.plugin.uiTweaks.streetView.enabled) window.plugin.uiTweaks.streetView.setup();
        
        $('#toolbox').append(window.plugin.uiTweaks.htmlCallSetBox);
    }
    
    // Sync
    window.addHook('pluginUiTweaksEdit', window.plugin.uiTweaks.sync.syncOptions);
    window.addHook('iitcLoaded', window.plugin.uiTweaks.sync.registerField);
    
    // Initialization done!
    window.plugin.uiTweaks.INIT = true;
  };

// PLUGIN END //////////////////////////////////////////////////////////


setup.info = plugin_info; //add the script info data to the function as a property
if(!window.bootPlugins) window.bootPlugins = [];
window.bootPlugins.push(setup);
// if IITC has already booted, immediately run the 'setup' function
if(window.iitcLoaded && typeof setup === 'function') setup();
} // wrapper end
// inject code into site context
var script = document.createElement('script');
var info = {};
if (typeof GM_info !== 'undefined' && GM_info && GM_info.script) info.script = { version: GM_info.script.version, name: GM_info.script.name, description: GM_info.script.description };
script.appendChild(document.createTextNode('('+ wrapper +')('+JSON.stringify(info)+');'));
(document.body || document.head || document.documentElement).appendChild(script);